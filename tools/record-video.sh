#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

OFFSET_X=2
OFFSET_Y=20
OFFSET_X_RIGHT=2
OFFSET_Y_BOTTOM=26
WIDTH=$((1920 - OFFSET_X - OFFSET_X_RIGHT))
HEIGHT=$((1080 - OFFSET_Y - OFFSET_Y_BOTTOM))
FRAMERATE=30
PULSE_INPUT_ID=${PULSE_INPUT_ID:-3}
FFMPEG_ARGS="-video_size ${WIDTH}x${HEIGHT} -framerate ${FRAMERATE} -f x11grab -i :0.0+${OFFSET_X},${OFFSET_Y} -f pulse -ac 2 -i ${PULSE_INPUT_ID} -c:v libx264rgb -crf 0 -preset ultrafast"

# TODO make a tmux running on the screen at right place
# TODO ^^ should also change hostname to hide that
killall -SIGUSR1 dunst
trap 'killall -SIGUSR2 dunst' INT
trap 'killall -SIGUSR2 dunst' EXIT
# shellcheck disable=SC2086
ffmpeg ${FFMPEG_ARGS} video.mkv
killall -SIGUSR2 dunst
