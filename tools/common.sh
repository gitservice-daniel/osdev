# shellcheck shell=sh

set -x  # show execution trace
set -eu # Fail on errors or bad variables
# shellcheck disable=SC2039
[ -n "${BASH+x}" ] && set -o pipefail

export SRC_DIR
export TOOLS_DIR
export BUILD_DIR
export BASE_DIR
export OS_NAME

SRC_DIR="$(dirname "$0")"/../src
TOOLS_DIR="$(dirname "$0")"
BUILD_DIR="$(dirname "$0")"/../build
BASE_DIR="$(dirname "$0")"/..
OS_NAME="osdev"
