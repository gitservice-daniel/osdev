#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

[ -z "$(git status --porcelain)" ] || {
    echo "git status is not clean. Not proceeding with version bump."
    exit 1
}

if [ -f "${BASE_DIR}"/VERSION ]; then
    OLD_VERSION="$(cat VERSION)"
    VERSION="$(echo "${OLD_VERSION}" | awk -F. -v OFS=. '{$NF += 1 ; print}')"
    sed -i "s/^## Bleeding Edge/## Version ${VERSION}/" "${BASE_DIR}"/CHANGELOG.md
    LINE_OF_OLD_VERSION=$(grep -n "${OLD_VERSION}" <CHANGELOG.md | cut -d ':' -f 1)
    LINE_OF_OLD_VERSION=$((LINE_OF_OLD_VERSION - 1))
    head -n"${LINE_OF_OLD_VERSION}" "${BASE_DIR}"/CHANGELOG.md >"${BASE_DIR}"/release-notes/"${VERSION}".md
    sed -i "1s/^/## Bleeding Edge\n\n/" "${BASE_DIR}"/CHANGELOG.md
else
    # No version has been released yet
    VERSION=0.0.0.1
    sed -i "s/^## Bleeding Edge/## Version ${VERSION}/" "${BASE_DIR}"/CHANGELOG.md
    mkdir "${BASE_DIR}"/release-notes || true
    cp "${BASE_DIR}"/CHANGELOG.md "${BASE_DIR}"/release-notes/"${VERSION}".md
    sed -i "1s/^/## Bleeding Edge\n\n/" "${BASE_DIR}"/CHANGELOG.md
fi
echo "${VERSION}" >"${BASE_DIR}"/VERSION
git add "${BASE_DIR}"/VERSION
git add "${BASE_DIR}"/CHANGELOG.md
git add "${BASE_DIR}"/release-notes/"${VERSION}".md
cp "${BASE_DIR}"/demo.gif "${BASE_DIR}"/release-notes/"${VERSION}".gif
git lfs track release-notes/"${VERSION}".gif
git add "${BASE_DIR}"/.gitattributes
git add "${BASE_DIR}"/release-notes/"${VERSION}".gif
git commit -m "Version bump to ${VERSION}"
git push # This will trigger CI to publish a release
