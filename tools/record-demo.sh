#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

run_tests() {

    xdotool type "echo hi there"
    xdotool key Return
    sleep 0.5

    xdotool type "test_colour"
    xdotool key Return
    sleep 0.5

    xdotool type "help"
    xdotool key Return
    sleep 0.5

    xdotool type "test_malloc"
    xdotool key Return
    sleep 0.5

    xdotool type "sched_test"
    xdotool key Return
    sleep 0.5

    xdotool type "music chord D#3 major"
    xdotool key Return
    sleep 2

    xdotool type "notacommand 1 2 3"
    xdotool key Return
    sleep 0.5

    xdotool type "echo    scrolling works"
    xdotool key Return
    sleep 2

    xdotool key Page_Up
    sleep 0.5
    xdotool key Page_Up
    sleep 0.5
    xdotool key Page_Up
    sleep 0.5
    xdotool key Page_Up
    sleep 0.5
    xdotool key Page_Up
    sleep 1

    xdotool key Page_Down
    sleep 0.5
    xdotool key Page_Down
    sleep 0.5
    xdotool key Page_Down
    sleep 0.5
    xdotool key Page_Down
    sleep 0.5
    xdotool key Page_Down
    sleep 0.5

    xdotool type "history"
    xdotool key Return
    sleep 0.5

    xdotool type "echo history also works with cursors"
    xdotool key Return
    sleep 1

    xdotool type "printf \e[5mGoodbye\ for\ now\e[0m"
    xdotool key Return
    sleep 0.5

}

WIDTH=740
HEIGHT=480
OFFSET_X=2
OFFSET_Y=20
FRAMERATE=30
FFMPEG_ARGS="-video_size ${WIDTH}x${HEIGHT} -framerate ${FRAMERATE} -f x11grab -i :0.0+${OFFSET_X},${OFFSET_Y} -c:v libx264rgb -crf 0 -preset ultrafast"
FFMPEG_GIF_ARGS="-r 4 -vf scale=640:-1"

"${TOOLS_DIR}/run-virtualbox.sh" &
OS_PID=$!
sleep 6
# shellcheck disable=SC2086
ffmpeg ${FFMPEG_ARGS} -y demo.mkv &
sleep 4
FFMPEG_PID=$!
run_tests
sleep 1
kill $FFMPEG_PID
kill $OS_PID
# shellcheck disable=SC2086
ffmpeg -i demo.mkv ${FFMPEG_GIF_ARGS} -y demo.gif
