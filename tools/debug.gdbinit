## Start qemu and hook into it
# TODO, use -readconfig, as well as in tools/run.sh
target remote | exec qemu-system-i386 -soundhw pcspk -drive file=build/osdev.img,format=raw -S -gdb stdio
python
def try_pass(arg):
    try:
        gdb.execute (arg)
    except:
        pass
end

define q
    # This fails if qemu closes before we quit
    python
try_pass ("monitor quit")
    end
    quit
end

## Show some useful layouts then focus the cmd pane

layout asm
layout regs
focus cmd

set logging on

## Prefer intel vs at&t
set disassembly-flavor intel

# Debug 16 bit real mode, or 32 bit protected mode
if 1 == 0
    ## boot: (boot/boot32.inc) (16 bit real mode)
    set architecture i8086
    break *0x7c00
    define hook-stop
        set $debug_break = *(short*)0x7c09
        echo Setting breakpoint for debug_break:\n
        break *$debug_break
        # Reset hook-stop
        define hook-stop
        end
    end
else
    # Need to break into here before we can read the rest of memory set up by boot
    echo Continuing to start of protected mode\n
    ## boot32: (boot/boot32.inc) (32 bit protected mode)
    break *0x7e00
    define hook-stop
        ## int3_handler: (boot/boot32.inc)
        # This is pointed to in a structure at the start of boot32
        set $int3_handler = *0x7e02
        echo Setting breakpoint for int3:\n
        break *$int3_handler

        set $busy_loop = *0x7e06
        echo Disabling busy_loop
        # 0xc3 = near ret (same segment)
        set *$busy_loop = 0xc3
        # Reset hook-stop
        define hook-stop
        end
    end
end

set $current_task = 0x6287e
set $id = 0
set $_esp = 8
set $next = 16
set $state = 20
set $first_task = 0xc33d
set $other_task = 0x1036f
display/dw (*$current_task + $id)
display/dw ($first_task + $id)
display/xw ($first_task + $_esp)
display/xw ($first_task + $next)
display/dw ($first_task + $state)
display/dw ($other_task + $id)
display/xw ($other_task + $_esp)
display/xw ($other_task + $next)
display/dw ($other_task + $state)
