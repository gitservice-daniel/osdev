#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

"${TOOLS_DIR}"/build.sh
gdb -q -x "${TOOLS_DIR}"/debug.gdbinit
