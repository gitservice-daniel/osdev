#!/bin/bash

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

KERNEL="${KERNEL:=asm}"
CC="cc"
CFLAGS="-m32 -nostdlib -ffreestanding -fno-pic -Wall -Wextra -Werror"
LDFLAGS="-melf_i386"
OBJS=""
# FIXME: The array format and prefix replacement is only in bash, but it is nice...
DRIVERS=(vga pic8259 ps2-keyboard pit)
LIBS=(string stdio ctype stdlib unistd music termios pthread)
UTILS=(shell music)
C_LIBS=()
TESTS=()
C_TESTS=(itoa atoi printf isdigit strcmp strlen puts strnrev putchar getchar fgets colour strtok malloc)
ASM_FILES="boot/boot ${DRIVERS[*]/#/drivers\/} ${LIBS[*]/#/lib\/} ${UTILS[*]/#/utils\/} ${TESTS[*]/#/tests\/}"
C_FILES="test ${C_LIBS[*]/#/lib\/} /${C_TESTS[*]/#/tests\/}"
if [ "${KERNEL}" = "c" ]; then
    C_FILES="${C_FILES} kernel"
else
    ASM_FILES="${ASM_FILES} kernel"
fi
ASM_FILES="${ASM_FILES} scheduler"
for FILE in ${ASM_FILES}; do
    fasm "${SRC_DIR}/${FILE}.asm"
    mkdir -p "$(dirname "${BUILD_DIR}/${FILE}")"
    mv "${SRC_DIR}/${FILE}.o" "${BUILD_DIR}/${FILE}.o"
    OBJS="$OBJS ${BUILD_DIR}/${FILE}.o"
done
for FILE in ${C_FILES}; do
    mkdir -p "$(dirname "${BUILD_DIR}/${FILE}")"
    # shellcheck disable=SC2086
    ${CC} ${CFLAGS} "${SRC_DIR}/${FILE}.c" -c -o "${BUILD_DIR}/${FILE}.o"
    OBJS="$OBJS ${BUILD_DIR}/${FILE}.o"
done
# shellcheck disable=SC2086
ld ${LDFLAGS} -T "${SRC_DIR}"/linker.ld ${OBJS} -o "${BUILD_DIR}/${OS_NAME}.img"
# Make it atleast 10M and align to 512 bytes
DISK_SIZE="$(stat -c '%s' "${BUILD_DIR}/${OS_NAME}.img")"
if [ "${DISK_SIZE}" -lt 10321920 ]; then
    truncate -s 10321920 "${BUILD_DIR}/${OS_NAME}.img"
else
    truncate -s $(((DISK_SIZE + 511) / 512 * 512)) "${BUILD_DIR}/${OS_NAME}.img"
fi
# shellcheck disable=SC2086
rm ${OBJS}
# TODO: clear up build dirs created (ie, build/boot) which remain empty once the .o is rm'd
