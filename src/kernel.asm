	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  kmain

	include 'lib/includes/stdio.inc'
	include 'lib/includes/string.inc'
	include 'includes/kernel.inc'

	extrn shell_main

kmain:
	enter 0, 0
	ccall puts, hello_str
	call  shell_main
	add   esp, 4

.return:
	leave
	ret

hello_str:
	db "Hello from kernel.asm", 10, 0
