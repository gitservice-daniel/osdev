#include "../lib/includes/stdio.h"
#include "../lib/includes/stdlib.h"
#include "test-macros.h"
#include <stddef.h>
// for size_t

typedef struct {
    char* str;
    int expected;
} test_args;

void test_atoi()
{
    test_args test_cases[] = {
        { "1", 1 },
        { "abcd", 0 },
        { "0", 0 },
        { "42", 42 },
        { "123", 123 },
        { "1337", 1337 },
        { "45678", 45678 },
        { "-1", -1 },
        { "-98", -98 },
        { "-127", -127 },
        { "-7946", -7946 },
        { "-19537", -19537 },
    };

    puts("Testing int atoi(char *str)\n");
    for (size_t i = 0; i < sizeof(test_cases) / sizeof(test_cases[0]); ++i) {
        test_args t = test_cases[i];
        TEST_OR_PANIC(atoi, t.expected, int, s, t.str);
    }
    puts("Finished testing itoa\n");
}
