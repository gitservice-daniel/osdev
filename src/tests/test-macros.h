// FIXME: this is not supported in clang

// for size_t
#include <stddef.h>
extern char* strncpy(char* dest, const char* src, size_t n);
extern int printf(const char* fmt, ...);
extern void panic();

extern void vga_enable_last_printed_str();
extern void vga_disable_last_printed_str();
extern char* vga_escape_str(const char* s);

#define ELEVENTH_ARGUMENT(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, ...) a11
#define COUNT_ARGUMENTS(...) ELEVENTH_ARGUMENT(dummy, ##__VA_ARGS__, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
#define CONCAT(a, b) a##b
#define CONCAT2(a, b) CONCAT(a, b)
#define CONCAT3(a, b, c) CONCAT2(CONCAT2(a, b), c)
#define TEST_FMT(arg_type, ...) CONCAT3(TEST_FMT_, arg_type, COUNT_ARGUMENTS(__VA_ARGS__))
#define TEST_FMT_s1 "\"%s\""
#define TEST_FMT_s2 "\"%s\", \"%s\""
#define TEST_FMT_x1 "%#x"
#define TEST_FMT_d1 "%#d"

// TODO: when vga_escape_str supports returning allocated mem, this will work as expected
#define VGA_ESCAPE_CAP 200
#define VGA_ESCAPE(arg_type, ...)                                \
    CONCAT3(VGA_ESCAPE_, arg_type, COUNT_ARGUMENTS(__VA_ARGS__)) \
    (__VA_ARGS__)
#define VGA_ESCAPE_s1(a) ({                          \
    char buf[VGA_ESCAPE_CAP];                        \
    strncpy(buf, vga_escape_str(a), VGA_ESCAPE_CAP); \
    buf;                                             \
})
#define VGA_ESCAPE_s2(a1, a2) ({                                                     \
    char buf1[VGA_ESCAPE_CAP];                                                       \
    strncpy(buf1, vga_escape_str(a1), VGA_ESCAPE_CAP);                               \
    buf1;                                                                            \
}),                                                                                  \
                              ({                                                     \
                                  char buf2[VGA_ESCAPE_CAP];                         \
                                  char ignore[VGA_ESCAPE_CAP];                       \
                                  (void)ignore;                                      \
                                  strncpy(buf2, vga_escape_str(a2), VGA_ESCAPE_CAP); \
                                  buf2;                                              \
                              })
#define VGA_ESCAPE_x1(a) a
#define VGA_ESCAPE_d1(a) a

// TODO: allow non string args
// TODO: have a diff colour for fail
#define TEST_OR_(exit_method, func, expected, ret_type, arg_type, ...) ({                                                                 \
    vga_enable_last_printed_str();                                                                                                        \
    ret_type i = func(__VA_ARGS__);                                                                                                       \
    vga_disable_last_printed_str();                                                                                                       \
    if (i != expected) {                                                                                                                  \
        printf("\tFAIL: " #func "(" TEST_FMT(arg_type, __VA_ARGS__) ") != %d, got %d\n", VGA_ESCAPE(arg_type, __VA_ARGS__), expected, i); \
        exit_method;                                                                                                                      \
    };                                                                                                                                    \
    printf("\tPASS: " #func "(" TEST_FMT(arg_type, __VA_ARGS__) ") == %d\n", VGA_ESCAPE(arg_type, __VA_ARGS__), expected);                \
    i;                                                                                                                                    \
})
#define TEST_STRCMP_OR_(exit_method, func, expected, arg_type, ...) ({                                                                                         \
    vga_enable_last_printed_str();                                                                                                                             \
    char* ret = func(__VA_ARGS__);                                                                                                                             \
    vga_disable_last_printed_str();                                                                                                                            \
    if (!expected && ret) {                                                                                                                                    \
        printf("\tret should be NULL, it is %p\n", ret);                                                                                                       \
    }                                                                                                                                                          \
    if ((!expected && ret) || strcmp(ret, expected) != 0) {                                                                                                    \
        printf("\tFAIL: strcmp(" #func "(" TEST_FMT(arg_type, __VA_ARGS__) "), \"%s\") != 0, got \"%s\"\n", VGA_ESCAPE(arg_type, __VA_ARGS__), expected, ret); \
        exit_method;                                                                                                                                           \
    };                                                                                                                                                         \
    printf("\tPASS: strcmp(" #func "(" TEST_FMT(arg_type, __VA_ARGS__) "), \"%s\") == 0\n", VGA_ESCAPE(arg_type, __VA_ARGS__), expected);                      \
    ret;                                                                                                                                                       \
})
#define TEST_STRN_OR_(exit_method, func, expected, s, n) ({                                              \
    vga_enable_last_printed_str();                                                                       \
    char* ret = func(s, n);                                                                              \
    vga_disable_last_printed_str();                                                                      \
    if (strcmp(ret, expected) != 0) {                                                                    \
        printf("\tFAIL: strcmp(" #func "(\"%s\", %d), \"%s\") != 0, got \"%s\"\n", s, n, expected, ret); \
        exit_method;                                                                                     \
    };                                                                                                   \
    printf("\tPASS: strcmp(" #func "(\"%s\", %d)), \"%s\") == 0\n", s, n, expected);                     \
    ret;                                                                                                 \
})
#define TEST_OR_PANIC(func, expected, ret_type, arg_type, ...) TEST_OR_(panic(), func, expected, ret_type, arg_type, __VA_ARGS__)
#define TEST_OR_BREAK(func, expected, ret_type, arg_type, ...) TEST_OR_(break, func, expected, ret_type, arg_type, __VA_ARGS__)
#define TEST_OR_CONTINUE(func, expected, ret_type, arg_type, ...) TEST_OR_(continue, func, expected, ret_type, arg_type, __VA_ARGS__)
#define TEST_OR_RETURN(func, expected, ret_type, arg_type, ...) TEST_OR_(return, func, expected, ret_type, arg_type, __VA_ARGS__)
#define TEST_STRCMP_OR_PANIC(func, expected, arg_type, ...) TEST_STRCMP_OR_(panic(), func, expected, arg_type, __VA_ARGS__)
#define TEST_STRCMP_OR_BREAK(func, expected, arg_type, ...) TEST_STRCMP_OR_(break, func, expected, arg_type, __VA_ARGS__)
#define TEST_STRCMP_OR_CONTINUE(func, expected, arg_type, ...) TEST_STRCMP_OR_(continue, func, expected, arg_type, __VA_ARGS__)
#define TEST_STRCMP_OR_RETURN(func, expected, arg_type, ...) TEST_STRCMP_OR_(return, func, expected, arg_type, __VA_ARGS__)
#define TEST_STRN_OR_PANIC(func, expected, s, n) TEST_STRN_OR_(panic(), func, expected, s, n)
#define TEST_STRN_OR_BREAK(func, expected, s, n) TEST_STRN_OR_(break, func, expected, s, n)
#define TEST_STRN_OR_CONTINUE(func, expected, s, n) TEST_STRN_OR_(continue, func, expected, s, n)
#define TEST_STRN_OR_RETURN(func, expected, s, n) TEST_STRN_OR_(return, func, expected, s, n)

#define GEN_TEST(func, arg_type, ...) \
    printf("        { " TEST_FMT(arg_type, __VA_ARGS__) ", %d },\n", __VA_ARGS__, func(__VA_ARGS__))
