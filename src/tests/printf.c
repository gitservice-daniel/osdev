#include "../lib/includes/stdio.h"
extern void panic();

void test_printf_panic(char* format, ...)
{
    puts("printf: Failed printing format:\n\t");
    puts(format);
    putnl();
    // TODO: get vargs
    panic();
}

#define TEST_PRINTF_OR_PANIC(format, args...) ({ \
    int i = printf(format, ##args);              \
    if (i < 0) {                                 \
        test_printf_panic(format, ##args);       \
    };                                           \
    i;                                           \
})

void test_printf()
{
    puts("Testing int printf(char *format, ...)\n");
    // TODO split this up into helpers
    // TODO test the return value
    // TODO maybe read the output somehow and test that...
    TEST_PRINTF_OR_PANIC("\tTesting printf with no format flags\n");
    TEST_PRINTF_OR_PANIC("\tTesting printf with escaped percent: %%\n");
    TEST_PRINTF_OR_PANIC("\tTesting printf with the string OK: %s\n", "OK");
    TEST_PRINTF_OR_PANIC("\tTesting printf with the hex number 0xCAFE: %x\n", 0xCAFE);
    TEST_PRINTF_OR_PANIC("\tTesting printf with the octal number 0753: %o\n", 0753);
    TEST_PRINTF_OR_PANIC("\tTesting printf with the signed decimal number -127: %d\n", -127);
    TEST_PRINTF_OR_PANIC("\tTesting printf with the signed decimal number 593: %d\n", 593);
    TEST_PRINTF_OR_PANIC("\tTesting printf with the unsigned decimal number -127: %u\n", -127);
    TEST_PRINTF_OR_PANIC("\tTesting printf with the unsigned decimal number 593: %u\n", 593);
    TEST_PRINTF_OR_PANIC("\tTesting printf with the multiple args, string, 0xABCD, percent, -57, (unsigned -57), percent, final string:\n%s, %x, %%, %d, %u, %%, %s\n",
        "string",
        0xABCD,
        -57,
        -57,
        "final string");
    TEST_PRINTF_OR_PANIC("\tTesting printf with signed decimal number -456 (with i not d) : %i\n", -456);
    TEST_PRINTF_OR_PANIC("\tTesting printf with upper hex: %X\n", 0xFACE);
    TEST_PRINTF_OR_PANIC("\tTesting printf with char x: %c\n", 'x');
    TEST_PRINTF_OR_PANIC("\tTesting printf with pointer to printf (should have 0x): %p\n", printf);
    // TODO: test cases for other formats (float etc)
    // TODO: test cases for %n
    // TODO: test return code of printf is number of chars printed
    // TODO: test with edge cases of numbers (max/min etc)
    TEST_PRINTF_OR_PANIC("\tTesting printf with octal 075 and format width 3: \"%3o\"\n", 075);
    TEST_PRINTF_OR_PANIC("\tTesting printf with hex 0xF and format width 3: \"%3x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with signed decimal -1 and format width 3: \"%3d\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with unsigned decimal -1 and format width 11: \"%11u\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with unsigned decimal 1 and format width 5: \"%5u\"\n", 1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with string \"abc\" and format width 5: \"%5s\"\n", "abc");
    TEST_PRINTF_OR_PANIC("\tTesting printf with char x and format width of 3: \"%3c\"\n", 'x');
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%08o (should be 00001234): %08o\n", 01234);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%#o (should be 01234): %#o\n", 01234);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%#8o (should be \"   01234\"): \"%#8o\"\n", 01234);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%#07o (should be 0001234): %#07o\n", 01234);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%08x (should be 0000face): %08x\n", 0xFACE);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%#x (should be 0xface): %#x\n", 0xFACE);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%#8x (should be \"  0xface\"): \"%#8x\"\n", 0xFACE);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%#010x (should be 0x0000face): %#010x\n", 0xFACE);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%5s\", abc: \"%5s\"\n", "abc");
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%-5s\", abc: \"%-5s\"\n", "abc");
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%-3c\", x: \"%-3c\"\n", 'x');
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%-3d\", -1: \"%-3d\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%03d\", -1: \"%03d\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%-#07x\": \"%-#07x\"\n", 0xFA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%#07x\": \"%#07x\"\n", 0xFA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%% x\": \"% x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%% 2x\": \"% 2x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%+x\": \"%+x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%+2x\": \"%+2x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%% +x\": \"% +x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%% +2x\": \"% +2x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%0 +2x\": \"%0 +2x\"\n", 0xF);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%0+3d\": \"%0+3d\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%0+3d\": \"%0+3d\"\n", 1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%0 3d\": \"%0 3d\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%0 3d\": \"%0 3d\"\n", 1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%+3d\": \"%+3d\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%+3d\": \"%+3d\"\n", 1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%% 3d\": \"% 3d\"\n", -1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%% 3d\": \"% 3d\"\n", 1);
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%+3s\": \"%+3s\"\n", "a");
    TEST_PRINTF_OR_PANIC("\tTesting printf with \"%%*d\" (5, 42): %*d\n", 5, 42);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hhx (0x123456): %hhx\n", 0x123456);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hhd (0x123456): %hhd\n", 0x123456);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hx (0x123456): %hx\n", 0x123456);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hd (0x123456): %hd\n", 0x123456);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hhx (0xFEDCBA): %hhx\n", 0xFEDCBA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hhd (0xFEDCBA): %hhd\n", 0xFEDCBA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hhu (0xFEDCBA): %hhu\n", 0xFEDCBA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hx (0xFEDCBA): %hx\n", 0xFEDCBA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hd (0xFEDCBA): %hd\n", 0xFEDCBA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%hu (0xFEDCBA): %hu\n", 0xFEDCBA);
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%.3s (abcdef): %.3s\n", "abcdef");
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%.*s (2, abcdef): %.*s\n", 2, "abcdef");
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%-5s abcdef: \"%-5s\"\n", "abcdef");
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%-5.5s abcdef: \"%-5.5s\"\n", "abcdef");
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%-5s abcdef: \"%-5s\"\n", "abc");

    // This is currently failing
    TEST_PRINTF_OR_PANIC("\tTesting printf with %%-5.5s abcdef: \"%-5.5s\"\n", "abc");
    //    TEST_PRINTF_OR_PANIC("\tTesting printf with %%.1x (0xabcdef): %.3x\n", 0xabcdef);
    //    TEST_PRINTF_OR_PANIC("\tTesting printf with %%.*x (2, 0xabcdef): %.*x\n", 2, 0xabcdef);
    // TODO: test cases for precision on numeric types
    // TODO: test cases for other confer
    // TODO: test cases for flags
    // TODO: test cases for precision
    // TODO: test case for length modifiers for long types
    // TODO: test cases for %*d and %2$d formats (or even %2$*1$d)
    puts("Finished testing printf\n");
}
