#include "../lib/includes/stdio.h"
#include "../lib/includes/string.h"
#include "test-macros.h"

typedef struct {
    char* s1;
    char* s2;
    int expected;
} test_args;

void test_strcmp()
{
    test_args test_cases[] = {
        // TODO: Make this work with <0, 0, >0
        { "", "", 0 },
        { "a", "", 1 },
        { "", "a", -1 },
        { "a", "a", 0 },
        { "abc", "a", 1 },
        { "a", "abc", -1 },
        { "ab", "abc", -1 },
        { "abc", "abc", 0 },
        { "dabc", "abc", 1 },
        { "abc", "dabc", -1 },
        { "this", "these", 1 },
    };

    puts("Testing int strcmp(const char *s1, const char *s2)\n");
    for (size_t i = 0; i < sizeof(test_cases) / sizeof(test_cases[0]); ++i) {
        test_args t = test_cases[i];
        TEST_OR_PANIC(strcmp, t.expected, int, s, t.s1, t.s2);
    }
    puts("Testing NULL\n");
    // this should be printing (null)
    // this is also getting garbage leaked
    TEST_OR_PANIC(strcmp, 0, int, s, NULL, NULL);
    TEST_OR_PANIC(strcmp, 1, int, s, "", NULL);
    TEST_OR_PANIC(strcmp, 1, int, s, "a", NULL);
    TEST_OR_PANIC(strcmp, -1, int, s, NULL, "");
    TEST_OR_PANIC(strcmp, -1, int, s, NULL, "a");
    puts("Finished testing strcmp\n");
}
