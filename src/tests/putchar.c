#include "../lib/includes/stdio.h"
#include "../lib/includes/string.h"
#include "test-macros.h"
#include <stddef.h>
// For size_t

// TODO: This is used to test what vga actually prints, feels hacky
extern char* vga_last_printed_str();

typedef struct {
    int c;
    int expected;
    char* expected_output;
} test_args;

void test_putchar()
{
    test_args test_cases[] = {
        // TODO: better tests for the \t, like compare to what the cursor actually does
        { '\t', '\t', "\t" },
        { '\n', '\n', "\n" },
        { '\r', '\r', "\r" },
        { '\'', '\'', "'" },
        { '\b', '\b', "\b" },
        { '\a', '\a', "\a" },
        { '\f', '\f', "\f" },
        { '\v', '\v', "\v" },
        { ' ', ' ', " " },
        { 'a', 'a', "a" },
        { '0', '0', "0" },
        { 'A', 'A', "A" },
        { '?', '?', "?" },
        { '\0', 0, "" },
    };

    puts("Testing int putchar(int)\n");
    for (size_t i = 0; i < sizeof(test_cases) / sizeof(test_cases[0]); ++i) {
        test_args t = test_cases[i];
        TEST_OR_PANIC(putchar, t.expected, int, x, t.c);
        char* printed = vga_last_printed_str();
        TEST_OR_PANIC(strcmp, 0, int, s, t.expected_output, printed);
    }
    puts("TODO: Test EOF\n");
    puts("Finished testing puts\n");
}
