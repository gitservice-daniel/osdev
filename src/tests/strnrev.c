#include "../lib/includes/stdio.h"
#include "../lib/includes/string.h"
#include "test-macros.h"
#include <stddef.h>
// For size_t

typedef struct {
    char* s;
    int n;
    char* expected;
} test_args;

void test_strnrev()
{
    test_args test_cases[] = {
        { "abc", 3, "cba" },
        { "abcdef", 3, "cbadef" },
        { "", 0, "" },
        { "", 7, NULL },
        { "ab", 9, NULL },
        { "", 5, NULL },
        { "abc", 0, "abc" },
    };

    puts("Testing char *strnrev(const char *s)\n");
    for (size_t i = 0; i < sizeof(test_cases) / sizeof(test_cases[0]); ++i) {
        test_args t = test_cases[i];
        TEST_STRN_OR_PANIC(strnrev, t.expected, t.s, t.n);
        // TODO test original wasn't changed (it currently is)
        // FIXME: because original is changed, the printf displays the updated str
        // TODO: in strn funcs, also strcpy the result to a temp, and strcmp ret against the original. Maybe this should be done in macros
    }
    puts("Testing NULL\n");
    TEST_STRN_OR_PANIC(strnrev, NULL, NULL, 0);
    TEST_STRN_OR_PANIC(strnrev, NULL, NULL, 1);
    puts("Finished testing strnrev\n");
}
