#include "../lib/includes/stdio.h"
#include "../lib/includes/stdlib.h"
#include "test-macros.h"

void test_malloc()
{
    puts("Testing void *malloc(size_t size)\n");
    void *p1, *p2;
    p1 = malloc(5);
    printf("p1=malloc(5) returned %p\n", p1);
    p2 = malloc(5);
    printf("p2=malloc(5) returned %p\n", p2);
    free(p1);
    p1 = malloc(5);
    printf("p1=malloc(5) after free(p1) should be the same: %p\n", p1);
    free(p1);
    p1 = malloc(10);
    printf("p1=malloc(10) after free(p1) should be after the p2: %p\n", p1);
    free(p2);
    p2 = malloc(10);
    printf("p2=malloc(10) after free(p2) should be at the start: %p\n", p2);
    free(p1);
    free(p2);
    puts("Testing 0 size\n");
    TEST_OR_PANIC(malloc, NULL, void*, d, 0);
    puts("Testing overflow\n");
    TEST_OR_PANIC(malloc, NULL, void*, d, 10000000);
    puts("Finished testing malloc\n");
}
