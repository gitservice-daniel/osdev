#include "../lib/includes/stdio.h"
#include "../lib/includes/string.h"
#include "test-macros.h"

typedef struct {
    char* str;
    const char* delim;
    char* expected;
} test_args;

void test_strtok()
{
    test_args test_cases[] = {
        { "aaa;;bbb,", ";,", "aaa" },
        { NULL, ";,", "bbb" },
        { NULL, ";,", NULL },
    };

    puts("Testing char *strtok(char *str, const char *delim)\n");
    for (size_t f = 0; f < sizeof(test_cases) / sizeof(test_cases[0]); ++f) {
        test_args t = test_cases[f];
        // Print test case before hand, as strtok destroys it
        if (t.str) {
            puts(t.str);
            putnl();
        }
        TEST_STRCMP_OR_PANIC(strtok, t.expected, s, t.str, t.delim);
    }
    puts("Finished testing strtok\n");
}
