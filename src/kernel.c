#include "includes/stdio.h"
#include "includes/stdlib.h"

void kmain()
{
    puts("Hello from kernel.c\n");
    char itoa_buf[33];
    char* str = itoa(0x1337BABE, itoa_buf, 16);
    if (str == 0) {
        puts("call to itoa from kernel.c failed\n");
    } else {
        puts(str);
        putnl();
    }
}
