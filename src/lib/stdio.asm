	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  puts
	public  putchar
	public  putnl
	public  printf
	public  getchar
	public  fgetc
	public  getc
	public  fgets
	public  stdin
	public  stdout
	public  stderr

	include '../includes/kernel.inc'
	include '../drivers/includes/vga.inc'
	include 'includes/stdlib.inc'
	include 'includes/ctype.inc'
	include 'includes/string.inc'
	include '../includes/ascii.inc'

	STDIO_BUF_CAP = 4095

stdin:
	dd    0; current length of buf
	dd    0; index into buf for ring buffer
	dd    STDIO_BUF_CAP
	times STDIO_BUF_CAP db 0; buffer

stderr:
stdout:
	dd    0; current length of buf
	dd    0; index into buf for ring buffer
	dd    STDIO_BUF_CAP
	times STDIO_BUF_CAP db 0; buffer

	;;     int getc(FILE* stream)
	stream equ arg1

fgetc:
getc:
	enter 0, 0
	push  ebx

	mov  ebx, stream
	test ebx, ebx
	jz   .eof

	xor    eax, eax
	;      TODO this should probably lock
	cmp    dword [ebx], 0
	je     .eof
	mov    edx, [ebx+WORD_SIZE]
	add    edx, ebx
	add    edx, 3*WORD_SIZE
	mov    al, [edx]
	dec    dword [ebx]
	inc    dword [ebx+WORD_SIZE]
	mov    ecx, 0
	mov    edx, [ebx+WORD_SIZE]
	cmp    edx, [ebx+3*WORD_SIZE]
	cmovge edx, ecx
	mov    [ebx+WORD_SIZE], edx
	jmp    .return

.eof:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;; int getchar()

getchar:
	enter 0, 0

	ccall getc, stdin

	leave
	ret

	;;     char *fgets(char *s, int size, FILE *stream)
	s      equ arg1
	size   equ arg2
	stream equ arg3

fgets:
	enter 0, 0
	push  edi

	mov edi, s
	mov ecx, size
	dec ecx

.getc:
	push  ecx
	mov   ecx, stream
	ccall getc, ecx
	pop   ecx
	cmp   eax, -1
	je    .eof
	stosb
	cmp   eax, ASCII_NEWLINE
	je    .success
	dec   ecx
	test  ecx, ecx
	jz    .success
	jmp   .getc

.eof:
	cmp edi, s
	jnz .success
	mov eax, 0
	jmp .return

.success:
	mov al, 0
	stosb
	mov eax, s

.return:
	pop edi
	leave
	ret

	; TODO make the vga stuff become stdout
	; maybe stderr can have an ansi code prepended when printed (so coloured)
	; vga should probably be flushed after adding to stdout

	;; int puts(char *)

puts:
	jmp vga_puts

	;; int putchar(char)

putchar:
	jmp vga_putchar

	;; void putnl(void)

putnl:
	jmp vga_putnl

	; TODO make fprintf, will need fputs and fputc

	;;     int printf(char *format, ...)
	;      args
	format equ arg1
	va_args_offset equ arg2_offset
	;      `...` gets stored in ebx, and incremented by WORD_SIZE

	;         variables
	width     equ var1
	padding_c     equ var2
	padding_order equ var3
	prefix    equ var4
	length_mask   equ var5
	precision equ var6
	args_size = 6 * WORD_SIZE

printf:
	enter args_size, 0
	push  esi
	push  ebx

	mov esi, format
	mov ebx, ebp
	add ebx, va_args_offset; current arg index

	; TODO check for NULL

.loop:
	xor   eax, eax
	lodsb
	or    al, al
	jz    .return
	cmp   eax, '%'
	je    .percent
	ccall putchar, eax
	jmp   .loop
	mov eax, 0
	jmp .return

.percent:
	;   TODO docs
	;   Set defaults
	;   padding_c, character to pad with
	;   padding_order
	;   0- at start
	;   1- between suffix and converted str
	;   2- at end
	;   prefix, string to print before the converted string (like 0x for hex)
	mov padding_c, ' '
	mov padding_order, 0
	mov prefix, empty_str
	mov length_mask, 0xFFFFFFFF
	mov precision, -1

.flags:
	;   read another char
	lodsb
	or  al, al
	jz  .error; Expected another char
	cmp eax, '0'
	je  .zero_padding_flag
	cmp eax, '#'
	je  .alternate_form_flag
	cmp eax, '-'
	je  .left_justified_flag
	cmp eax, ' '
	je  .space_flag
	cmp eax, '+'
	je  .positive_flag
	;   TODO: SUS also defines ', and glibc has I
	;   Not a flag, test for width with same char
	jmp .field_width

.zero_padding_flag:
	cmp padding_order, 2
	;   0 flag is ignored if - flag is present
	je  .flags
	mov padding_c, '0'
	cmp prefix, 0
	jne .flags
	;   If using alternate form, then pad inbetween prefix and converted str
	mov padding_order, 1
	jmp .flags

.alternate_form_flag:
	;   set prefix to NULL, it is set later once we know what the conversion is
	mov prefix, 0
	cmp padding_c, '0'
	jne .flags
	;   If 0 padded (and left justify removes this flag), then pad inbetween prefix and converted str
	mov padding_order, 1
	jmp .flags

.left_justified_flag:
	mov padding_order, 2
	;   0 flag is ignored if - flag is present
	mov padding_c, ' '
	jmp .flags

.space_flag:
	cmp prefix, positive_prefix_str
	je  .flags
	;   + flag overrides the space flag
	mov prefix, space_prefix_str
	cmp padding_c, '0'
	jne .flags
	;   If 0 padded (and left justify removes this flag), then pad inbetween prefix and converted str
	mov padding_order, 1
	jmp .flags

.positive_flag:
	mov prefix, positive_prefix_str
	cmp padding_c, '0'
	jne .flags
	;   If 0 padded (and left justify removes this flag), then pad inbetween prefix and converted str
	mov padding_order, 1
	jmp .flags

.field_width:
	mov   width, 0
	cmp   al, '*'
	je    .field_width_star
	push  eax
	ccall isdigit, eax
	test  eax, eax
	pop   eax
	jz    .precision

.chop_width:
	dec   esi
	ccall atoi, esi
	inc   esi
	mov   width, eax

.chop_width_loop:
	lodsb
	or    al, al
	jz    .error; Expected something after the digits
	push  eax
	ccall isdigit, eax
	test  eax, eax
	pop   eax
	jnz   .chop_width_loop
	jmp   .precision

.field_width_star:
	;   TODO test next char for digit, *m$ format
	mov eax, [ebx]
	mov width, eax
	add ebx, WORD_SIZE
	lodsb
	or  al, al
	jz  .error; Expected something after the star
	jmp .precision

.precision:
	cmp   eax, '.'
	jne   .length_modifier
	lodsb
	or    al, al
	jz    .error; Expected something after the .
	mov   precision, 0
	cmp   al, '*'
	je    .precision_star
	push  eax
	ccall isdigit, eax
	test  eax, eax
	pop   eax
	jz    .length_modifier

.chop_precision:
	dec   esi
	ccall atoi, esi
	inc   esi
	mov   precision, eax

.chop_precision_loop:
	lodsb
	or    al, al
	jz    .error; Expected something after the digits
	push  eax
	ccall isdigit, eax
	test  eax, eax
	pop   eax
	jnz   .chop_precision_loop
	jmp   .length_modifier

.precision_star:
	;   TODO test next char for digit, *m$ format
	mov eax, [ebx]
	mov precision, eax
	add ebx, WORD_SIZE
	lodsb
	or  al, al
	jz  .error; Expected something after the star
	jmp .length_modifier

.length_modifier:
	cmp eax, 'h'
	je  .length_h
	;   TODO others
	jmp .conversion_specifiers

.length_h:
	lodsb
	or  al, al
	jz  .error; Expected something after the h
	cmp eax, 'h'
	je  .length_hh
	mov length_mask, 0xFFFF
	jmp .conversion_specifiers

.length_hh:
	lodsb
	or  al, al
	jz  .error; Expected something after the hh
	mov length_mask, 0xFF
	jmp .conversion_specifiers

.conversion_specifiers:
	cmp eax, 's'
	je  .percent_s
	cmp eax, 'x'
	je  .percent_x
	cmp eax, 'X'
	je  .percent_X
	cmp eax, 'o'
	je  .percent_o
	cmp eax, 'd'
	je  .percent_d
	cmp eax, 'i'
	je  .percent_i
	cmp eax, 'u'
	je  .percent_u
	cmp eax, 'c'
	je  .percent_c
	cmp eax, 'p'
	je  .percent_p
	cmp eax, '%'
	je  .percent_percent
	;   TODO add stubs
	;   Non standard
	cmp eax, 't'; Binary output
	je  .percent_t
	;   Invalid escape code
	jmp .error

.percent_s:
	mov   prefix, empty_str
	cmp   dword [ebx], 0
	je    .null_string
	pushd [ebx]
	add   ebx, WORD_SIZE
	jmp   .puts_with_length

.null_string:
	;     HACK for now, print (null) if null
	;     TODO test for debugging flag
	ccall puts, .null_str

	;   Ignore it, go to next token
	add ebx, WORD_SIZE
	jmp .loop

.null_str:
	db "(null)", 0

.percent_t:
	;    Non standard, binary output
	mov  eax, length_mask
	and  [ebx], eax
	push 2
	cmp  prefix, 0
	je   .percent_t_alternate
	mov  prefix, empty_str
	jmp  .itoa_puts

.percent_t_alternate:
	mov prefix, bin_prefix_str
	jmp .itoa_puts

.percent_x:
	mov  eax, length_mask
	and  [ebx], eax
	push 16
	cmp  prefix, 0
	je   .percent_x_alternate
	mov  prefix, empty_str
	jmp  .itoa_puts

.percent_x_alternate:
	mov prefix, hex_prefix_str
	jmp .itoa_puts

.percent_o:
	mov  eax, length_mask
	and  [ebx], eax
	push 8
	cmp  prefix, 0
	je   .percent_o_alternate
	mov  prefix, empty_str
	jmp  .itoa_puts

.percent_o_alternate:
	mov prefix, octal_prefix_str
	jmp .itoa_puts

.percent_d:
.percent_i:
	mov ecx, length_mask
	and [ebx], ecx
	mov eax, [ebx]
	cmp ecx, 0xFFFF
	je  .percent_d_sign_extend_h
	cmp ecx, 0xFF
	je  .percent_d_sign_extend_hh
	;   TODO support more
	jmp .percent_d_extended

.percent_d_sign_extend_h:
	movsx ecx, ax
	mov   [ebx], ecx
	jmp   .percent_d_extended

.percent_d_sign_extend_hh:
	movsx ecx, al
	mov   [ebx], ecx
	jmp   .percent_d_extended

.percent_d_extended:
	cmp dword [ebx], 0
	jge .percent_d_positive
	mov prefix, negative_prefix_str
	cmp padding_c, '0'
	jne .percent_d_positive
	mov padding_order, 1

.percent_d_positive:
	push 10
	jmp  .itoa_puts

.percent_X:
	mov  eax, length_mask
	and  [ebx], eax
	push 16
	cmp  prefix, 0
	je   .percent_X_alternate
	mov  prefix, empty_str
	jmp  .itoA_puts

.percent_X_alternate:
	mov prefix, hex_prefix_str
	jmp .itoA_puts

.percent_u:
	mov   eax, length_mask
	and   [ebx], eax
	ccall utoa, [ebx], itoa_temp_str, 10
	add   ebx, WORD_SIZE
	push  eax
	jmp   .puts_with_length

.percent_p:
	mov prefix, 0
	jmp .percent_x

.percent_c:
	;   TODO assert if padding_order is out of range
	cmp padding_order, 0
	jl  .error
	;   %c doesn't have a prefix, so 1 is error here
	cmp padding_order, 1
	je  .error
	cmp padding_order, 2
	jg  .error

	mov prefix, empty_str
	;   consume the character in any minimum width
	dec width

	cmp  padding_order, 0
	jne  .putchar
	call .padding

.putchar:
	;     %c doesn't have a prefix, so skip 1
	dec   padding_order
	ccall putchar, [ebx]
	add   ebx, WORD_SIZE
	;     TODO check error code
	dec   padding_order
	cmp   padding_order, 0
	jne   .loop
	call  .padding
	jmp   .loop

.percent_percent:
	;     NOTE: %% does not have minimum width
	ccall putchar, '%'
	jmp   .loop

	; TODO check for null prefix for a, A, e, E, f, F, g, and G

.itoA_puts:
	mov edx, itoA
	jmp .itoa_puts_

.itoa_puts:
	mov edx, itoa
	jmp .itoa_puts_

.itoa_puts_:
	push  itoa_temp_str
	pushd [ebx]
	add   ebx, WORD_SIZE
	;     Can't use ccall here as the base arg was pushed in .percent_[x, d, o, X, t]
	call  edx
	add   esp, 3*WORD_SIZE
	;     TODO check for errors
	cmp   byte [eax], '-'
	jne   .not_negative
	inc   eax

.not_negative:
	push eax
	cmp  precision, 0
	;    TODO precision for numeric types is diff to strings
	jge  .error
	jmp  .puts_with_length

.puts_with_length:
	;   TODO assert if padding_order is out of range
	cmp padding_order, 0
	jl  .error
	cmp padding_order, 2
	jg  .error

	mov   edx, prefix
	ccall strlen, edx
	sub   width, eax
	mov   eax, [esp]
	ccall strlen, eax
	sub   width, eax

	;    Duplicate stack top, it will be used in padding (bounds check above)
	cmp  padding_order, 0
	jne  .puts_prefix
	;    Can't use ccall here as the str arg was pushed in .percent_u, or .itoa_puts_
	call .padding
	;    don't add to esp, will reuse arg in puts

.puts_prefix:
	mov   edx, prefix
	ccall puts, edx
	dec   padding_order
	cmp   padding_order, 0
	jne   .puts
	call  .padding
	;     don't add to esp, will reuse arg in puts

.puts:
	cmp  precision, 0
	jge  .puts_with_precision
	;    Can't use ccall here as the str arg was pushed in .percent_u, or .itoa_puts_
	call puts
	add  esp, WORD_SIZE
	;    TODO check error code
	dec  padding_order
	cmp  padding_order, 0
	jne  .loop
	;    Get the string back on the stack
	sub  esp, WORD_SIZE
	call .padding
	add  esp, WORD_SIZE
	jmp  .loop

.puts_with_precision:
	pop   ecx
	xor   eax, eax
	mov   al, [ecx]
	cmp   al, 0
	je    .loop
	cmp   precision, 0
	je    .loop
	push  ecx
	ccall putchar, eax
	pop   ecx
	inc   ecx
	push  ecx
	dec   precision
	jmp   .puts_with_precision

	; inner function, use call

.padding:
.padding_loop:
	cmp   width, 0
	jle   .end_padding
	mov   edx, padding_c
	ccall putchar, edx
	dec   width
	jmp   .padding_loop

.end_padding:
	ret

.error:
	;   TODO diff error cases
	mov eax, -1

.return:
	;   TODO return value
	pop ebx
	pop esi
	leave
	ret

	; TODO bss

itoa_temp_str:
	times 33 db 0; Enough for 32 bit platforms

empty_str:
	db "", 0

bin_prefix_str:
	db "0b", 0; Non standard %#t flag

hex_prefix_str:
	db "0x", 0

octal_prefix_str:
	db "0", 0

negative_prefix_str:
	db "-", 0

space_prefix_str:
	db " ", 0

positive_prefix_str:
	db "+", 0
