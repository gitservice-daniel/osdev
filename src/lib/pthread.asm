	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  pthread_spin_init
	public  pthread_spin_destroy
	public  pthread_spin_lock
	public  pthread_spin_trylock
	public  pthread_spin_unlock

	include '../includes/kernel.inc'

	;; int pthread_spin_init(pthread_spinlock_t *lock, int pshared)

	lck     equ arg1
	pshared equ arg2

pthread_spin_init:
	enter 0, 0
	mov   eax, lck

	;   For now, just treat *lock as an *int, where bit 0 being 0 is locked, and 1 is unlocked
	;   no need for lock prefix here, as init isn't a critical region
	mov dword [eax], 0

	; todo pshared

.return:
	leave
	ret

	;; int pthread_spin_destroy(pthread_spinlock_t *lock)

	lck equ arg1

pthread_spin_destroy:
	enter 0, 0

	; todo, if init ever allocates resources, they should be free'd here

.return:
	leave
	ret

	;; int pthread_spin_lock(pthread_spinlock_t *lock)

	lck equ arg1

pthread_spin_lock:
	enter 0, 0
	mov   eax, lck

.lock:
	lock bts dword [eax], 0
	jc   .spin; lock is not available, spin
	jmp  .return; lock is now ours

.spin:
	pause
	bt  dword [eax], 0
	jnz .spin; lock is not available, spin
	jmp .lock; lock is free, try acquire it

.return:
	;   todo error cases?
	xor eax, eax
	leave
	ret

	;; int pthread_spin_trylock(pthread_spinlock_t *lock)

	lck equ arg1

pthread_spin_trylock:
	enter 0, 0
	mov   eax, lck

	lock bts dword [eax], 0
	jc   .error
	xor  eax, eax
	jmp  .return

.error:
	;   TODO should set errno
	mov eax, -1

.return:
	leave
	ret

	;; int pthread_spin_unlock(pthread_spinlock_t *lock)

	lck equ arg1

pthread_spin_unlock:
	enter 0, 0
	mov   eax, lck

	;   TODO check we own the lock
	mov dword [eax], 0

	xor eax, eax

.return:
	leave
	ret
