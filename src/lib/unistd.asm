	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  sleep
	public  usleep

	extrn pit_ms
	extrn pit_fraction

	include '../includes/kernel.inc'

	;;      unsigned int sleep(unsigned int seconds)
	seconds equ arg1

sleep:
	enter 0, 0

	mov eax, seconds
	mov ecx, 1000
	mul ecx

	;     todo, we lose precision in edx, if seconds is more than 49 days...
	ccall usleep, eax

	leave
	ret

	;;   int usleep(useconds_t usec)
	usec equ arg1

usleep:
	enter 0, 0
	push  ebx
	push  esi

	cli
	mov ebx, [pit_ms]
	mov esi, [pit_ms+WORD_SIZE]
	sti
	;   todo should we care about pit_fractions for usleep, may need it in nanosleep
	;   usleep would probably call nanosleep then

	add ebx, usec
	adc esi, 0

.loop:
	;   todo, some concurrency locks needed here if also writing from pit_handler?
	;   for now, cli, but then we lose ticks?
	cli
	mov eax, [pit_ms+WORD_SIZE]
	cmp eax, esi
	jl  .wait
	jg  .success
	;   lower higher 64 bits is eq
	mov eax, [pit_ms]
	cmp eax, ebx
	jge .success

.wait:
	;   todo, some concurrency locks needed here if also writing from pit_handler?
	;   for now, cli, but then we lose ticks?
	sti
	nop
	nop
	nop
	nop
	nop
	nop
	jmp .loop

.success:
	sti
	;   todo should return number of seconds left if interrupted by a signal
	;   for now, return 0, as no concept of signal handlers
	mov eax, 0
	;   fall through

.return:
	pop esi
	pop ebx
	leave
	ret
