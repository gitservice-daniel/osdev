	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  tcgetattr
	public  tcsetattr
	public  termios_update_inbuf
	include '../includes/kernel.inc'
	include '../includes/ascii.inc'
	include '../lib/includes/termios_constants.inc'
	include '../lib/includes/unistd.inc'
	include '../lib/includes/stdio.inc'
	include '../lib/includes/string.inc'
	include '../lib/includes/pthread.inc'

	TERMIOS_BUF_CAP = 4095
	DEFAULT_LFLAG = ICANON or ECHO

	; TODO other fd's

c_lflag:
	dd DEFAULT_LFLAG

	fd equ arg1
	termios_p equ arg2
	;; int tcgetattr(int fd, struct termios *termios_p)

tcgetattr:
	enter 0, 0

	;   TODO other fd's
	cmp dword fd, STDIN_FILENO
	jne .error

	;   there is a virtual at eax defined in termios_constants.inc
	mov eax, termios_p

	;   TODO other struct members
	;   TODO other fd's
	mov edx, [c_lflag]
	mov [termios.c_lflag], edx

.success:
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	leave
	ret

	fd equ arg1
	optional_actions equ arg2
	termios_p equ arg3
	;; int tcsteattr(int fd, int optional_actions, const struct termios *termios_p)

tcsetattr:
	enter 0, 0
	ccall pthread_spin_lock, termios_raw_buf_lock

	;   TODO other fd's
	cmp dword fd, STDIN_FILENO
	jne .error

	;   TODO other action modes
	cmp dword optional_actions, TCSANOW
	jne .error

	;   there is a virtual at eax defined in termios_constants.inc
	mov eax, termios_p

	;   TODO other struct members
	mov edx, [termios.c_lflag]
	;   TODO other fd's
	mov [c_lflag], edx

.success:
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	push  eax
	ccall pthread_spin_unlock, termios_raw_buf_lock
	pop   eax
	leave
	ret

	;; void termios_update_inbuf(int fd, char c)
	fd equ arg1
	c  equ arg2

termios_update_inbuf:
	enter 0, 0
	push  ebx

	ccall pthread_spin_lock, termios_raw_buf_lock

	;   TODO diff fd's
	cmp dword fd, STDIN_FILENO
	jne .fd_error

	mov ebx, c

	test dword [c_lflag], ICANON
	jz   .add_to_stdin_buf

	;   Canonical mode, buffer in raw buffer
	cmp bl, ASCII_BACKSPACE
	je  .backspace
	cmp bl, ASCII_NEWLINE
	je  .move_raw_buf_to_stdin_buf
	;   delete makes no sense with no ability for cursor to go back
	;   escape makes no sense in canon mode

	jmp .add_to_raw_buf

.backspace:
	cmp dword [termios_raw_buf], 0
	je  .return
	dec dword [termios_raw_buf]
	jmp .echo

.move_raw_buf_to_stdin_buf:
	cmp dword [termios_raw_buf], 0
	je  .add_to_stdin_buf
	mov eax, [termios_raw_buf]
	add eax, [stdin]
	cmp eax, [stdin+2*WORD_SIZE]
	jle .memcpy
	;   Trim the input as overflow
	sub eax, TERMIOS_BUF_CAP
	sub [termios_raw_buf], eax

.memcpy:
	mov eax, [termios_raw_buf]
	xor edx, edx
	mov eax, [stdin]
	add eax, [stdin+WORD_SIZE]
	mov ecx, [stdin+2*WORD_SIZE]
	div ecx
	;   edx has idx of end of current stdin ring buffer
	;   need to copy from here to end of cap, and also from start if need to wrap around
	mov eax, [termios_raw_buf]
	add eax, edx
	cmp eax, TERMIOS_BUF_CAP
	jge .wrap
	;   can copy the whole buffer without wrapping

	sub   eax, [termios_raw_buf]
	add   eax, stdin + 3*WORD_SIZE
	ccall memcpy, eax, termios_raw_buf+WORD_SIZE, [termios_raw_buf]
	cmp   eax, 0
	jl    .memcpy_error
	;     clear the current buffer
	mov   edx, [termios_raw_buf]
	add   [stdin], edx
	mov   dword [termios_raw_buf], 0

	;   copy the current char to the end
	jmp .add_to_stdin_buf

.wrap:
	; need to work out how much to copy to the end, then to copy to start

	sub   eax, TERMIOS_BUF_CAP
	mov   edx, [termios_raw_buf]
	sub   edx, eax
	push  edx
	mov   eax, stdin + 3*WORD_SIZE
	add   eax, [stdin]
	add   eax, [stdin+WORD_SIZE]
	ccall memcpy, eax, termios_raw_buf+WORD_SIZE, edx
	cmp   eax, 0
	pop   edx
	jl    .memcpy_error
	add   [stdin], edx
	sub   [termios_raw_buf], edx

	add   edx, termios_raw_buf+WORD_SIZE
	ccall memcpy, stdin + 2*WORD_SIZE, edx, [termios_raw_buf]
	cmp   eax, 0
	jl    .memcpy_error
	mov   edx, [termios_raw_buf]
	add   [stdin], edx
	mov   dword [termios_raw_buf], 0
	;     copy the current char to the end
	jmp   .add_to_stdin_buf

.memcpy_error:
	ccall printf, .memcpy_printf_str
	call  panic
	;     UNREACHABLE
	cli
	hlt

.memcpy_printf_str:
	db "ERROR, memcpy failed in ps2_keyboard_update_buf.add_buffer_to_stdin_buffer", 10, 0

.add_to_raw_buf:

	cmp dword [termios_raw_buf], TERMIOS_BUF_CAP
	jge .echo
	mov edx, termios_raw_buf
	add edx, WORD_SIZE
	add edx, [termios_raw_buf]
	inc dword [termios_raw_buf]
	mov [edx], bl

	jmp .echo

.add_to_stdin_buf:
	mov edx, [stdin]
	cmp edx, [stdin+2*WORD_SIZE]
	jge .echo

	;   TODO implement some locking?
	xor edx, edx
	mov eax, [stdin]
	add eax, [stdin+WORD_SIZE]
	mov ecx, [stdin+2*WORD_SIZE]
	div ecx
	add edx, stdin + 3*WORD_SIZE
	inc dword [stdin]
	mov [edx], bl

.echo:
	test dword [c_lflag], ECHO
	jz   .return

	cmp bl, ASCII_ESCAPE
	je  .echo_escape
	;   TODO vt or xterm escapes

	ccall putchar, ebx

	jmp .return

.echo_escape:
	ccall puts, .escape_str
	jmp   .return

.escape_str:
	db "^[", 0

.return:
	ccall pthread_spin_unlock, termios_raw_buf_lock

	pop ebx
	leave
	ret

.fd_error:
	ccall puts, .fd_error_str
	extrn panic
	ccall panic

.fd_error_str:
	db "termios.asm, termios_update_inbuf, Unknown file descriptor", 13, 10, 0

termios_raw_buf_lock:
	dd 0

termios_raw_buf:
	dd    0; current length of buf
	times TERMIOS_BUF_CAP db 0; buffer
