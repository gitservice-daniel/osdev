	extrn stdin
	extrn stdout
	extrn stderr
	;;    int printf(char *, ...)
	extrn printf
	;;    int putchar(char)
	extrn putchar
	;;    int puts(char *)
	extrn puts
	;;    void putnl(void)
	extrn putnl
	;;    int getchar()
	extrn getchar
	;;    int fgetc(FILE* stream)
	extrn fgetc
	;;    int getc(FILE* stream)
	extrn getc
	;;    char *fgets(char *s, int size, FILE *stream)
	extrn fgets
