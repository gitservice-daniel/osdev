extern unsigned int sleep(unsigned int seconds);
typedef unsigned int useconds_t;
extern int usleep(useconds_t usec);

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
