	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  itoa
	public  utoa
	public  itoA
	public  atoi
	public  malloc
	public  free

	include '../includes/kernel.inc'
	include 'includes/string.inc'
	include 'includes/stdio.inc'
	include 'includes/ctype.inc'

	;;    char *itoa(int value, char *str, int base)
	;;    itoa is signed for base 10, and unsigned for other bases
	;;    utoa is unsigned
	;     args
	value equ arg1
	str   equ arg2
	base  equ arg3

	;        variables
	p        equ var1
	unsigned_flag equ var2
	alphabet equ var3
	args_size = 3 * WORD_SIZE

utoa:
	enter args_size, 0
	mov   unsigned_flag, 1
	mov   alphabet, itoa_alpha_str
	jmp   _itoa

itoa:
	enter args_size, 0
	mov   unsigned_flag, 0
	mov   alphabet, itoa_alpha_str
	jmp   _itoa

itoA:
	enter args_size, 0
	mov   unsigned_flag, 0
	mov   alphabet, itoa_ALPHA_str
	jmp   _itoa

_itoa:

	; TODO check for NULL in str

	mov   eax, base
	cmp   eax, 2
	jl    .error
	cmp   eax, 36
	jg    .error
	mov   eax, unsigned_flag
	and   eax, eax
	jnz   .not_if_base10_neg
	mov   eax, base
	cmp   eax, 10
	jnz   .not_if_base10_neg
	mov   eax, value
	cmp   eax, 0
	jge   .not_if_base10_neg
	neg   eax
	mov   ecx, str
	mov   byte [ecx], '-'
	inc   ecx
	mov   edx, base
	ccall itoa, eax, ecx, edx
	mov   eax, str
	jmp   .return

.not_if_base10_neg:
	mov  eax, str
	test eax, eax
	jz   .error
	;    p = str
	mov  eax, str
	mov  p, eax

.divmod_loop:
	;    value, digit = divmod(value, offset)
	mov  edx, 0
	mov  eax, value
	mov  ecx, base
	div  ecx
	mov  value, eax
	;    *(p++) = alpha[digit]
	mov  eax, alphabet
	add  eax, edx
	mov  dl, [eax]
	mov  eax, p
	mov  [eax], dl
	inc  eax
	mov  p, eax
	;    while (value != 0)
	mov  eax, value
	test eax, eax
	jz   .end_loop
	jmp  .divmod_loop

.end_loop:
	;     *p = 0
	mov   eax, p
	mov   dword [eax], 0
	sub   eax, str
	mov   ecx, str
	ccall strnrev, ecx, eax
	mov   eax, str
	jmp   .return

.error:
	mov eax, 0

.return:
	leave
	ret

	;;  int atoi(char *str)
	str equ arg1

atoi:
	enter 0, 0
	push  ebx
	push  esi
	push  edi
	mov   esi, str

	; TODO check for NULL

	mov ecx, 0
	mov edi, 1
	cmp byte [esi], '-'
	je  .negative
	jmp .loop

.negative:
	mov edi, -1
	inc esi

.loop:
	xor   eax, eax
	lodsb
	or    al, al
	jz    .return
	mov   ebx, eax
	ccall isdigit, eax
	test  eax, eax
	jz    .return
	sub   ebx, '0'
	mov   eax, 10
	xchg  eax, ecx
	mul   ecx
	add   eax, ebx
	xchg  eax, ecx
	jmp   .loop

.return:
	mov eax, ecx
	mul edi
	pop edi
	pop esi
	pop ebx
	leave
	ret

itoa_alpha_str:
	db "0123456789abcdefghijklmnopqrstuvwxyz", 0

itoa_ALPHA_str:
	db "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0

	;       TODO some better structures like AVL or red/black tree
	;       for now, doubly linked list
	struc   malloc_chunk {
	.used   db ?
	.length dd ?
	.next   dd ?
	.prev   dd ?
	}
	;       TODO make a struct/ends macro to hide the need for virtual
	;       see "https://stackoverflow.com/questions/41843715/defining-a-structure-in-fasm-which-of-the-2-ways-is-better-in-what-situation#48058223"
	virtual at eax
	malloc_chunk malloc_chunk
	sizeof.malloc_chunk = $ - malloc_chunk
	end     virtual

malloc_init:
	enter 0, 0
	mov   eax, malloc_heap
	mov   [malloc_chunk.used], 0
	mov   [malloc_chunk.length], MALLOC_CAP - sizeof.malloc_chunk
	mov   [malloc_chunk.next], 0
	mov   [malloc_chunk.prev], 0
	leave
	ret

	size equ arg1
	;;   void *malloc(size_t size)

malloc:
	enter 0, 0
	push  ebx
	push  esi

	mov   eax, malloc_heap
	cmp   [malloc_chunk.length], 0
	jne   .initialized
	ccall malloc_init

.initialized:

	mov  ecx, size
	test ecx, ecx
	jz   .error

	mov eax, malloc_heap

	; TODO in future, this lookup won't be sequential and will use a binary search tree of some kind (avl or red/black most likely)

.loop:
	test eax, eax
	jz   .error

	test [malloc_chunk.used], 1
	jnz  .loop_inc

	cmp [malloc_chunk.length], ecx
	jl  .loop_inc

	jmp .alloc

.loop_inc:
	mov eax, [malloc_chunk.next]
	jmp .loop

.alloc:
	not  [malloc_chunk.used]
	mov  ebx, eax
	mov  edx, [malloc_chunk.length]
	sub  edx, sizeof.malloc_chunk
	sub  edx, ecx
	test edx, edx
	jl   .after_split; no room to split this chunk

	mov esi, [malloc_chunk.next]
	add eax, ecx
	add eax, sizeof.malloc_chunk
	mov [malloc_chunk.length], edx
	mov [malloc_chunk.next], esi
	mov [malloc_chunk.prev], ebx
	mov esi, eax
	mov eax, ebx
	mov [malloc_chunk.length], ecx
	mov [malloc_chunk.next], esi

.after_split:
	add eax, sizeof.malloc_chunk
	jmp .return

.error:
	xor eax, eax

.return:
	pop esi
	pop ebx
	leave
	ret

	ptr equ arg1
	;;  void free(void *ptr)

free:
	enter 0, 0
	mov   eax, ptr
	;     TODO bounds check

	;   TODO in future, this will be done with better data struc such as hashtable to lookup the chunk
	;   for now it is inline
	sub eax, sizeof.malloc_chunk
	not [malloc_chunk.used]
	;   TODO join chunks

	leave
	ret

	MALLOC_CAP = 4096*32

malloc_heap:
	times MALLOC_CAP db 0
