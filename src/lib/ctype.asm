	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  isdigit

	include '../includes/kernel.inc'

	;; int isdigit(int c)
	c  equ arg1

isdigit:
	enter 0, 0
	cmp   dword c, '0'
	jl    .fail
	cmp   dword c, '9'
	jg    .fail
	mov   eax, 1
	jmp   .return

.fail:
	mov eax, 0
	;   fall through

.return:
	leave
	ret
