	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  vga_init
	public  vga_putchar
	public  vga_puts
	public  vga_putnl

	;      TODO: This is used by tests, find a better way
	public vga_last_printed_str
	public vga_enable_last_printed_str
	public vga_disable_last_printed_str
	public vga_escape_str

	include '../lib/includes/music.inc'
	include '../lib/includes/string.inc'
	include '../lib/includes/stdio.inc'
	include '../lib/includes/unistd.inc'
	include '../includes/kernel.inc'
	include '../includes/ascii.inc'
	include '../includes/csi.inc'

	extrn busy_loop

	VGA_BUFFER = 0xb8000
	VGA_WIDTH = 80
	VGA_HEIGHT = 25
	VGA_LINE_LEN = VGA_WIDTH * 2
	VGA_BUFFER_LEN = VGA_LINE_LEN * VGA_HEIGHT

	VGA_SCROLL_LINES_CAP = 1024
	VGA_SCROLL_BUF_CAP = VGA_SCROLL_LINES_CAP * VGA_LINE_LEN

	VGA_CHAR_BUF_CAP = VGA_WIDTH * VGA_HEIGHT ; Used to test what is actually printed
	VGA_ESCAPE_BUF_CAP = VGA_WIDTH * VGA_HEIGHT * 2 ; Used to return an escaped string, for test debugging

	VGA_CSI_ARGS_CAP = 256 + 1 ; +1 for the #args

	VGA_COLOUR_BLACK         = 0
	VGA_COLOUR_BLUE          = 1
	VGA_COLOUR_GREEN         = 2
	VGA_COLOUR_CYAN          = 3
	VGA_COLOUR_RED           = 4
	VGA_COLOUR_MAGENTA       = 5
	VGA_COLOUR_BROWN         = 6
	VGA_COLOUR_LIGHT_GREY    = 7
	VGA_COLOUR_DARK_GREY     = 8
	VGA_COLOUR_LIGHT_BLUE    = 9
	VGA_COLOUR_LIGHT_GREEN   = 10
	VGA_COLOUR_LIGHT_CYAN    = 11
	VGA_COLOUR_LIGHT_RED     = 12
	VGA_COLOUR_LIGHT_MAGENTA = 13
	VGA_COLOUR_LIGHT_BROWN   = 14
	VGA_COLOUR_WHITE         = 15

vga_default_fg:
	db VGA_COLOUR_WHITE

vga_default_bg:
	db VGA_COLOUR_GREEN

	; Escape code uses 16 bit table, map it through memory to lookup later

vga_csi_sgr_colours:
	db VGA_COLOUR_BLACK
	db VGA_COLOUR_RED
	db VGA_COLOUR_GREEN
	db VGA_COLOUR_BROWN
	db VGA_COLOUR_BLUE
	db VGA_COLOUR_MAGENTA
	db VGA_COLOUR_CYAN
	db VGA_COLOUR_LIGHT_GREY; can make this white with the bold (fg), or blink (bg on some vga's) bits

	NUM_SPACES_PER_TAB = 4

	VGA_NORMAL_MODE = 0
	VGA_ESCAPE_MODE = 1
	VGA_CSI_MODE    = 2

	VGA_PORT_REGISTER_C_COMMAND = 0x3D4
	VGA_PORT_REGISTER_C_DATA = 0x3D5
	VGA_PORT_CURSOR_START_INDEX = 0x0A
	VGA_PORT_CURSOR_END_INDEX = 0x0B
	VGA_PORT_MAX_SCANLINE_INDEX = 0x09
	VGA_PORT_CURSOR_LOC_HIGH_INDEX = 0x0E
	VGA_PORT_CURSOR_LOC_LOW_INDEX = 0x0F

	VGA_PORT_REGISTER_MISC_OUTPUT_WRITE = 0x3C2
	VGA_PORT_REGISTER_MISC_OUTPUT_READ = 0x3CC

	;; void vga_init()

vga_init:
	enter 0, 0
	call  vga_reset
	call  vga_set_io_address_select
	ccall vga_set_max_scanline, 0x0F
	ccall vga_set_cursor_size, 0x00, 0x0F
	mov   byte [vga_char_buf], 0
	mov   dword [vga_char_buf_idx], 0
	mov   byte [vga_enable_char_buf], 0
	mov   byte [vga_escape_mode], VGA_NORMAL_MODE
	leave
	ret

	;; void vga_putnl()

vga_putnl:
	enter 0, 0
	ccall vga_putchar, ASCII_NEWLINE
	leave
	ret

vga_last_printed_str:
	enter 0, 0
	mov   ecx, vga_char_buf
	add   dword ecx, [vga_char_buf_idx]
	mov   byte [ecx], 0
	mov   dword [vga_char_buf_idx], 0
	mov   eax, vga_char_buf
	leave
	ret

vga_enable_last_printed_str:
	mov byte [vga_enable_char_buf], 1
	ret

vga_disable_last_printed_str:
	mov byte [vga_enable_char_buf], 0
	ret

	;;  char *vga_escape_str(char *str)
	str equ arg1

vga_escape_str:
	enter 0, 0
	push  esi
	push  edi
	xor   eax, eax
	xor   ecx, ecx
	mov   esi, str
	;     TODO FIXME: allocate memory
	mov   edi, vga_escape_buf

.loop:
	;    todo check not out of bounds of buf
	test esi, esi
	jz   .return
	lodsb
	cmp  al, ASCII_ESCAPE
	je   .escape
	cmp  al, ASCII_NEWLINE
	je   .newline
	cmp  al, ASCII_CARRIAGE_RETURN
	je   .carriage
	cmp  al, ASCII_TAB
	je   .tab
	cmp  al, ASCII_BELL
	je   .bell
	cmp  al, ASCII_BACKSPACE
	je   .backspace
	cmp  al, ASCII_DELETE
	je   .delete
	cmp  al, ASCII_FORM_FEED
	je   .form_feed
	cmp  al, ASCII_VERTICAL_TAB
	je   .vertical_tab
	stosb
	or   al, al
	jz   .return
	inc  ecx
	jmp  .loop

.escape:
	mov al, '\'
	stosb
	mov al, 'e'
	stosb
	jmp .loop

.newline:
	mov al, '\'
	stosb
	mov al, 'n'
	stosb
	jmp .loop

.carriage:
	mov al, '\'
	stosb
	mov al, 'r'
	stosb
	jmp .loop

.tab:
	mov al, '\'
	stosb
	mov al, 't'
	stosb
	jmp .loop

.bell:
	mov al, '\'
	stosb
	mov al, 'a'
	stosb
	jmp .loop

.backspace:
	mov al, '\'
	stosb
	mov al, 'b'
	stosb
	jmp .loop

.delete:
	mov al, '\'
	stosb
	mov al, 'x'
	stosb
	mov al, '7'
	stosb
	mov al, 'f'
	stosb
	jmp .loop

.form_feed:
	mov al, '\'
	stosb
	mov al, 'f'
	stosb
	jmp .loop

.vertical_tab:
	mov al, '\'
	stosb
	mov al, 'v'
	stosb
	jmp .loop

.return:
	pop edi
	pop esi
	mov eax, vga_escape_buf
	leave
	ret

	;; int vga_putchar(char)
	c  equ arg1

vga_putchar:
	enter 0, 0
	push  ebx
	mov   eax, c
	cmp   byte [vga_enable_char_buf], 0
	je    .skip_char_buf
	cmp   dword [vga_char_buf_idx], VGA_CHAR_BUF_CAP
	jge   .skip_char_buf
	mov   ecx, vga_char_buf
	add   dword ecx, [vga_char_buf_idx]
	mov   byte [ecx], al
	inc   dword [vga_char_buf_idx]

.skip_char_buf:
	cmp   byte [vga_escape_mode], VGA_ESCAPE_MODE
	je    .escape_mode
	cmp   byte [vga_escape_mode], VGA_CSI_MODE
	je    .csi_mode
	cmp   eax, ASCII_ESCAPE
	je    .escape
	cmp   eax, ASCII_NEWLINE; line feed
	je    .newline
	cmp   eax, ASCII_CARRIAGE_RETURN
	je    .carriage
	cmp   eax, ASCII_TAB
	je    .tab
	cmp   eax, ASCII_BELL
	je    .bell
	cmp   eax, ASCII_BACKSPACE
	je    .backspace
	cmp   eax, ASCII_DELETE
	je    .delete
	cmp   eax, ASCII_FORM_FEED
	je    .form_feed
	cmp   eax, ASCII_VERTICAL_TAB
	je    .vertical_tab
	;     TODO check it is a printable char
	mov   edx, c
	ccall vga_putchar_at, edx
	mov   al, [vga_x]
	inc   al
	cmp   al, VGA_WIDTH
	je    .newline
	mov   [vga_x], al
	jmp   .success

.newline:
	mov   al, [vga_y]
	inc   al
	mov   [vga_y], al
	cmp   al, VGA_HEIGHT
	jne   .carriage
	dec   byte [vga_y]
	mov   eax, [vga_print_offset]
	cmp   eax, [vga_scroll_offset]
	pushf
	inc   eax
	;     TODO check overflow of buffer
	mov   [vga_print_offset], eax
	popf
	jne   .carriage
	ccall vga_scroll, 1
	;     fall through to carriage (so \n is equiv to \r\n (or even \n\r))

.carriage:
	mov byte [vga_x], 0
	jmp .success

.tab:
	xor  eax, eax
	mov  al, [vga_enable_char_buf]
	push eax
	mov  byte [vga_enable_char_buf], 0
	mov  ebx, NUM_SPACES_PER_TAB

.loop:
	ccall vga_putchar, " "
	dec   ebx
	test  ebx, ebx
	je    .end_loop
	jmp   .loop

.end_loop:
	pop eax
	mov [vga_enable_char_buf], al
	jmp .success

.bell:
	;     TODO customise, could flash or something instead?
	;     todo configurable frequency
	;     todo configurable duration
	ccall music_play_note, MUSIC_NOTE_C_4, 1
	jmp   .success

.backspace:
	cmp   byte [vga_x], 0
	je    .success
	dec   byte [vga_x]
	ccall vga_putchar, " "
	dec   byte [vga_x]
	jmp   .success

.delete:
	;   todo implement, should clear byte at x+1, but not move cursor
	;   only makes sense if we can move char back
	jmp .success

.form_feed:
	;    TODO need to do something diff here with scrolling
	;    I think this needs to add VGA_HEIGHT to print_offset
	call vga_clear_screen
	jmp  .success

.vertical_tab:
	;     Just do a linefeed without carriage return
	mov   al, [vga_y]
	inc   al
	mov   [vga_y], al
	cmp   al, VGA_HEIGHT
	jne   .success
	dec   byte [vga_y]
	mov   eax, [vga_print_offset]
	cmp   eax, [vga_scroll_offset]
	pushf
	inc   eax
	;     TODO check overflow of buffer
	mov   [vga_print_offset], eax
	popf
	jne   .success
	ccall vga_scroll, 1
	jmp   .success

.escape:
	mov byte [vga_escape_mode], VGA_ESCAPE_MODE
	jmp .success

.escape_mode:
	;   if c (eax) is 0x40-0x5F then it is an Fe escape code
	cmp eax, 0x40
	jl  .not_fe_escape
	cmp eax, 0x5F
	jg  .not_fe_escape
	jmp .fe_escape

.not_fe_escape:
	;   if c (eax) is 0x60—0x7E then it is an Fs escape code
	cmp eax, 0x60
	jl  .not_fs_escape
	cmp eax, 0x7E
	jg  .not_fs_escape
	jmp .fs_escape

.not_fs_escape:
	;   Unknown escape code
	jmp .error

.fe_escape:
	;   Currently just support CSI (Control Sequence Introducer)
	cmp eax, CSI_DELIM_CHAR
	je  .csi
	;   Unknown escape code
	jmp .error

.fs_escape:
	cmp eax, 'c'
	je  .ris; Reset to Initial State
	;   Unknown escape code
	jmp .error

.csi:
	;   Escape code \e[, Control Sequence Introducer
	mov byte [vga_escape_mode], VGA_CSI_MODE
	jmp .success
	; TODO make this a helper function to make this function tidier

.csi_mode:
	;    If c (eax) is 0x40–0x7E, then it is terminating char
	cmp  eax, 0x40
	jl   .csi_mode_parameters
	cmp  eax, 0x7E
	jg   .csi_mode_parameters
	;    terminating char ends escape sequence
	mov  byte [vga_escape_mode], VGA_NORMAL_MODE
	;    add current arg
	mov  ecx, [vga_csi_mode_args]
	inc  ecx
	mov  dword [vga_csi_mode_args], 0; Reset, but keep in ecx
	push eax
	mov  eax, ecx
	mov  edx, WORD_SIZE
	mul  edx
	mov  edx, [vga_csi_mode_current_param]
	mov  [vga_csi_mode_args + eax], edx
	mov  dword [vga_csi_mode_current_param], 0
	pop  eax
	;    Find correct function
	cmp  eax, CSI_SGR
	je   .csi_mode_sgr
	cmp  eax, CSI_CURSOR_UP
	je   .csi_mode_cursor_up
	cmp  eax, CSI_CURSOR_DOWN
	je   .csi_mode_cursor_down
	cmp  eax, CSI_CURSOR_FORWARD
	je   .csi_mode_cursor_forward
	cmp  eax, CSI_CURSOR_BACK
	je   .csi_mode_cursor_back
	cmp  eax, CSI_CURSOR_NEXT_LINE
	je   .csi_mode_cursor_next_line
	cmp  eax, CSI_CURSOR_PREV_LINE
	je   .csi_mode_cursor_prev_line
	cmp  eax, CSI_CURSOR_HORIZ
	je   .csi_mode_cursor_horiz
	cmp  eax, CSI_CURSOR_POSITION
	je   .csi_mode_cursor_position
	cmp  eax, CSI_SCROLL_UP
	je   .csi_mode_scroll_up
	cmp  eax, CSI_SCROLL_DOWN
	je   .csi_mode_scroll_down
	cmp  eax, CSI_CURSOR_HORIZ_VERT_POSITION
	je   .csi_mode_cursor_position; This is the same as \e[H
	cmp  eax, CSI_DEVICE_STATUS_REPORT
	je   .csi_mode_device_status_report

	;   TODO, support more
	jmp .error

.csi_mode_parameters:
	;   If it is 0x30-3F, then it is parameter
	cmp eax, 0x30
	jl  .csi_mode_intermediate_byte
	cmp eax, 0x3F
	jg  .csi_mode_intermediate_byte
	cmp eax, ';'
	je  .csi_mode_param_split
	;   We only support; as separator for now

	;   Check if 0-9 (0x30 is 0, checked above)
	cmp eax, '9'
	jg  .error
	sub eax, '0'
	mov ecx, eax
	mov edx, 10
	mov eax, [vga_csi_mode_current_param]
	mul edx
	add eax, ecx
	mov [vga_csi_mode_current_param], eax
	jmp .success

.csi_mode_param_split:
	;    we have to push this to a special stack
	mov  ecx, [vga_csi_mode_args]
	inc  ecx
	mov  [vga_csi_mode_args], ecx
	push eax
	mov  eax, ecx
	mov  edx, WORD_SIZE
	mul  edx
	mov  edx, [vga_csi_mode_current_param]
	mov  [vga_csi_mode_args + eax], edx
	mov  dword [vga_csi_mode_current_param], 0
	pop  eax
	jmp  .success

.csi_mode_intermediate_byte:
	;   It it isn't 0x20–0x2F, then not a valid CSI code
	cmp eax, 0x20
	jl  .error
	cmp eax, 0x2F
	jg  .error
	;   todo impl intermediate bytes
	;   for now, ignore them
	;   we should also check that no normal params come after?
	jmp .success

.csi_mode_sgr:
	;   number of args is in ecx
	mov edx, vga_csi_mode_args
	add edx, WORD_SIZE

.csi_mode_sgr_loop:
	test ecx, ecx
	jz   .success
	mov  eax, [edx]
	cmp  eax, 0
	je   .csi_mode_sgr_reset
	cmp  eax, 1
	je   .csi_mode_sgr_bright
	cmp  eax, 5
	je   .csi_mode_sgr_blink
	cmp  eax, 22
	je   .csi_mode_sgr_nobright
	cmp  eax, 25
	je   .csi_mode_sgr_noblink
	cmp  eax, 30
	jl   .not_if_csi_mode_sgr_set_foreground_colour
	cmp  eax, 37
	jg   .not_if_csi_mode_sgr_set_foreground_colour
	sub  eax, 30
	mov  al, [vga_csi_sgr_colours + eax]
	mov  ah, [vga_bright_bit]
	or   al, ah
	mov  [vga_fg_colour], al
	jmp  .csi_mode_sgr_loop_inc

.not_if_csi_mode_sgr_set_foreground_colour:
	cmp eax, 40
	jl  .not_if_csi_mode_sgr_set_background_colour
	cmp eax, 47
	jg  .not_if_csi_mode_sgr_set_background_colour
	sub eax, 40
	;   TODO The same bright bit is used for fore and background on some vga (like qemu), but bg bright is blink on others (like bochs)
	mov al, [vga_csi_sgr_colours + eax]
	mov ah, [vga_blink_bit]
	or  al, ah
	mov [vga_bg_colour], al
	jmp .csi_mode_sgr_loop_inc

.not_if_csi_mode_sgr_set_background_colour:
	;   No other codes supported yet
	jmp .error

.csi_mode_sgr_loop_inc:
	add edx, WORD_SIZE
	dec ecx
	jmp .csi_mode_sgr_loop

.csi_mode_sgr_reset:
	call vga_reset_colours
	jmp  .csi_mode_sgr_loop_inc

.csi_mode_sgr_bright:
	mov al, [vga_fg_colour]
	or  al, 0x8
	mov [vga_fg_colour], al
	;   TODO The same bright bit is used for fore and background on some vga (like qemu), but bg bright is blink on others (like bochs)
	mov byte [vga_bright_bit], 0x8
	jmp .csi_mode_sgr_loop_inc

.csi_mode_sgr_blink:
	mov al, [vga_bg_colour]
	or  al, 0x8
	mov [vga_bg_colour], al
	mov byte [vga_blink_bit], 0x8
	jmp .csi_mode_sgr_loop_inc

.csi_mode_sgr_nobright:
	mov al, [vga_fg_colour]
	and al, 0x7
	mov [vga_fg_colour], al
	;   TODO The same bright bit is used for fore and background on some vga (like qemu), but bg bright is blink on others (like bochs)
	mov byte [vga_bright_bit], 0
	jmp .csi_mode_sgr_loop_inc

.csi_mode_sgr_noblink:
	mov al, [vga_bg_colour]
	and al, 0x7
	mov [vga_bg_colour], al
	mov byte [vga_blink_bit], 0
	jmp .csi_mode_sgr_loop_inc

.csi_mode_cursor_up:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	sub   dl, [vga_y]
	neg   dl
	mov   cx, 0
	cmp   dl, cl
	cmovl dx, cx
	mov   [vga_y], dl
	jmp   .success

.csi_mode_cursor_down:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	add   dl, [vga_y]
	mov   cx, VGA_HEIGHT - 1
	cmp   dl, cl
	cmovg dx, cx
	mov   [vga_y], dl
	jmp   .success

.csi_mode_cursor_forward:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	add   dl, [vga_x]
	mov   cx, VGA_WIDTH - 1
	cmp   dl, cl
	cmovg dx, cx
	mov   [vga_x], dl
	jmp   .success

.csi_mode_cursor_back:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	sub   dl, [vga_x]
	neg   dl
	mov   cx, 0
	cmp   dl, cl
	cmovl dx, cx
	mov   [vga_x], dl
	jmp   .success

.csi_mode_cursor_next_line:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	add   dl, [vga_y]
	mov   cx, VGA_HEIGHT - 1
	cmp   dl, cl
	cmovg dx, cx
	mov   [vga_y], dl
	mov   byte [vga_x], 0
	jmp   .success

.csi_mode_cursor_prev_line:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	sub   dl, [vga_y]
	neg   dl
	mov   cx, 0
	cmp   dl, cl
	cmovl dx, cx
	mov   [vga_y], dl
	mov   byte [vga_x], 0
	jmp   .success

.csi_mode_cursor_horiz:
	;     assume just one arg, default to 1, 1-based index
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	dec   dl
	mov   cx, VGA_WIDTH - 1
	cmp   dl, cl
	cmovg dx, cx
	mov   [vga_x], dl
	jmp   .success

.csi_mode_cursor_position:
	;     assume two args, default to 1, 1 based indices
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + 1*WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	dec   dl
	mov   cx, VGA_HEIGHT - 1
	cmp   dl, cl
	cmovg dx, cx
	mov   [vga_y], dl
	mov   edx, [vga_csi_mode_args + 2*WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	dec   dl
	mov   cx, VGA_WIDTH - 1
	cmp   dl, cl
	cmovg dx, cx
	mov   [vga_x], dl
	jmp   .success

.csi_mode_scroll_up:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	neg   edx
	ccall vga_scroll, edx
	jmp   .success

.csi_mode_scroll_down:
	;     assume just one arg, default to 1
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	test  edx, edx
	lahf  ; load flags into ah
	shr   ah, 6
	and   ah, 0x1; Get zero flag
	add   dl, ah
	ccall vga_scroll, edx
	jmp   .success

.csi_mode_device_status_report:
	;     todo assert arg length
	mov   edx, [vga_csi_mode_args + WORD_SIZE]
	cmp   edx, 6
	jne   .success
	xor   eax, eax
	xor   ecx, ecx
	mov   cl, [vga_x]
	mov   al, [vga_y]
	ccall printf, .device_status_report_printf_str, eax, ecx
	jmp   .success
.device_status_report_printf_str: db "\e[%hhu;%hhuR", 0

.ris:
	;    Reset to initial state
	mov  byte [vga_escape_mode], VGA_NORMAL_MODE
	call vga_reset
	call vga_clear_screen
	jmp  .success

.error:
	mov byte [vga_escape_mode], VGA_NORMAL_MODE
	mov eax, -1
	jmp .return

.success:
	call vga_update_cursor_location
	;    Return the character printed
	;    TODO: do we want this if in escape code?
	mov  eax, c

.return:
	pop ebx
	leave
	ret

	; TODO make this read from stdin and stdout

	;;  int vga_puts(char *)
	str equ arg1

vga_puts:
	enter 0, 0
	push  esi
	xor   eax, eax
	xor   ecx, ecx
	mov   esi, str
	test  esi, esi
	jnz   .loop
	mov   eax, -1
	jmp   .return

.loop:
	lodsb
	or    al, al
	jz    .end
	push  ecx
	ccall vga_putchar, eax
	pop   ecx
	cmp   eax, 0
	jl    .return
	inc   ecx
	jmp   .loop

.end:
	mov eax, ecx
	jmp .return

.return:
	pop esi
	leave
	ret

	;; Not exported helper functions

	;; void vga_putchar_at(char c)
	c  equ arg1

vga_putchar_at:
	enter 0, 0
	pushfd
	cli
	xor   eax, eax
	mov   al, [vga_y]
	mov   ecx, VGA_WIDTH
	mul   ecx
	xor   ecx, ecx
	mov   cl, [vga_x]
	add   eax, ecx
	shl   eax, 1
	add   eax, VGA_BUFFER
	mov   edx, eax
	mov   cl, c
	mov   ch, [vga_fg_colour]
	mov   al, [vga_bg_colour]
	shl   al, 4
	or    al, ch
	mov   ch, al
	;     TODO implement scroll lock
	mov   eax, [vga_scroll_offset]
	cmp   eax, [vga_print_offset]
	jne   .skip_display
	mov   word [edx], cx

.skip_display:
	sub  edx, VGA_BUFFER
	add  edx, vga_scroll_buf
	push edx
	mov  eax, [vga_print_offset]
	mov  edx, VGA_LINE_LEN
	mul  edx
	pop  edx
	add  edx, eax
	mov  word [edx], cx

.return:
	pop  ecx
	test ecx, 0x200
	jz   .no_interrupts
	sti

.no_interrupts:
	;    todo remove this at some point
	call busy_loop
	leave
	ret

	;;     void vga_scroll(int lines)
	;      TODO accept arg, as amount to scroll by
	offset equ arg1

vga_scroll:
	enter 0, 0
	push  ebx
	push  esi
	push  edi

	xor edx, edx
	mov eax, offset
	add eax, [vga_scroll_offset]
	cmp eax, [vga_print_offset]
	jg  .stop_at_print_offset
	;   TODO this .overflow should be done in a sep place?
	cmp eax, VGA_SCROLL_LINES_CAP - VGA_HEIGHT; can we view a whole screen here
	jge .overflow
	cmp eax, 0
	jge .memcpy_vga
	;   underflow, reset to 0 silently
	xor eax, eax
	jmp .memcpy_vga

.stop_at_print_offset:
	mov eax, [vga_print_offset]

.memcpy_vga:
	mov   [vga_scroll_offset], eax
	mov   ecx, VGA_LINE_LEN
	mul   ecx
	add   eax, vga_scroll_buf
	;     copy a screen from current scroll offset into VGA buffer
	ccall memcpy, VGA_BUFFER, eax, VGA_BUFFER_LEN
	jmp   .return

.overflow:
	;   TODO this .overflow should be done in a sep place?
	;   memcpy doesn't allow overlaps, so do it by row
	;   memmove also copies to temp buffer first, which is slow
	;   for (int y = 0; y < VGA_SCROLL_LINES_CAP - offset; y ++)
	;   memcpy(vga_scroll_buf[y*VGA_LINE_LEN], vga_scroll[(y+offset)*VGA_LINE_LEN], VGA_LINE_LEN)
	mov ebx, VGA_LINE_LEN
	xor ecx, ecx

.loop:
	mov eax, ecx
	mul ebx
	add eax, vga_scroll_buf
	mov edi, eax

	mov eax, ecx
	add eax, offset
	mul ebx
	add eax, vga_scroll_buf
	mov esi, eax

	ccall memcpy, edi, esi, VGA_LINE_LEN
	inc   ecx
	mov   edx, VGA_SCROLL_LINES_CAP
	sub   edx, offset
	cmp   ecx, edx
	jl    .loop
	mov   eax, offset
	mul   ebx
	mov   ecx, eax
	mov   edi, VGA_SCROLL_BUF_CAP - 1
	sub   edi, ecx
	add   edi, vga_scroll_buf
	ccall memset, edi, 0, ecx
	;     Show the last page in scroll (will overflow next time)
	mov   eax, VGA_SCROLL_LINES_CAP - VGA_HEIGHT - 1
	;     TODO move print_offset
	jmp   .memcpy_vga

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;; void vga_clear_screen()

vga_clear_screen:
	;     TODO if having a scroll buffer, then this should scroll VGA_HEIGHT times
	mov   byte [vga_x], 0
	mov   byte [vga_y], 0
	ccall memset, VGA_BUFFER, 0, VGA_WIDTH * VGA_HEIGHT * 2
	ret

vga_reset:
	mov  byte [vga_x], 0
	mov  byte [vga_y], 0
	call vga_reset_colours
	ret

vga_reset_colours:
	mov al, [vga_default_fg]
	mov [vga_fg_colour], al
	mov al, [vga_default_bg]
	mov [vga_bg_colour], al
	mov byte [vga_bright_bit], 0
	mov byte [vga_blink_bit], 0
	ret

vga_set_io_address_select:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_MISC_OUTPUT_READ
	in    al, dx
	or    al, 0x1
	mov   dx, VGA_PORT_REGISTER_MISC_OUTPUT_WRITE
	out   dx, al
	leave
	ret

	;;  void vga_set_max_scanline(int val)
	val equ arg1

vga_set_max_scanline:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_MAX_SCANLINE_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	or    al, val
	out   dx, al
	leave
	ret

	;;    void vga_set_cursor_size(int start, int end)
	start equ arg1
	end   equ arg2

vga_set_cursor_size:
	enter 0, 0
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_START_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	and   al, 0xC0
	or    al, start
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_END_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	in    al, dx
	and   al, 0xE0
	or    al, end
	out   dx, al
	leave
	ret

	;; void vga_update_cursor_location

vga_update_cursor_location:
	enter 0, 0
	xor   eax, eax
	mov   al, [vga_y]
	mov   ecx, VGA_WIDTH
	mul   ecx
	xor   ecx, ecx
	mov   cl, [vga_x]
	add   eax, ecx
	mov   ecx, eax
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_LOC_LOW_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	mov   al, cl
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_COMMAND
	mov   al, VGA_PORT_CURSOR_LOC_HIGH_INDEX
	out   dx, al
	mov   dx, VGA_PORT_REGISTER_C_DATA
	mov   al, ch
	out   dx, al
	leave
	ret

section '.bss'

vga_x:
	db 0

vga_y:
	db 0

vga_fg_colour:
	db 0

vga_bg_colour:
	db 0

vga_bright_bit:
	db 0

vga_blink_bit:
	db 0

vga_char_buf:
	times VGA_CHAR_BUF_CAP db 0

vga_char_buf_idx:
	dd 0

vga_enable_char_buf:
	db 0

vga_escape_buf:
	times VGA_ESCAPE_BUF_CAP db 0

vga_escape_mode:
	db 0

vga_csi_mode_current_param:
	dd 0

vga_csi_mode_args:
	times VGA_CSI_ARGS_CAP dd 0

vga_scroll_buf:
	times VGA_SCROLL_BUF_CAP db 0

vga_scroll_offset:
	dd 0

vga_print_offset:
	dd 0
