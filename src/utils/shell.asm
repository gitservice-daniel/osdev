	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  shell_main

	include "../includes/kernel.inc"
	include "../lib/includes/stdio.inc"
	include "../lib/includes/stdlib.inc"
	include "../lib/includes/string.inc"
	include "../lib/includes/unistd.inc"
	include "../lib/includes/termios.inc"
	include "../includes/ascii.inc"
	include "../includes/csi.inc"

	BUF_CAP = 4096
	; FIXME Increasing this causes the kernel to not be able to load everything..
	HISTORY_CAP = 50

	stream equ arg1
	echo_char   equ arg2

	escape_mode equ var1
	args_size = 1 * WORD_SIZE

	ESCAPE_MODE_NORMAL = 0
	ESCAPE_MODE_ESCAPE = 1
	ESCAPE_MODE_CSI    = 2

shell_process_command:
	enter args_size, 0
	push  ebx
	push  esi
	push  edi

	ccall memset, shell_command_buffer, NULL, BUF_CAP
	mov   escape_mode, 0

	xor ecx, ecx

.history_loop:
	cmp ecx, HISTORY_CAP
	jge .history_done
	cmp dword [history_list + WORD_SIZE*ecx], 0
	je  .history_done
	inc ecx
	jmp .history_loop

.history_done:
	mov esi, ecx

	;   TODO, for history and other fun stuff
	;   set raw mode with tcsetattr
	;   use getc(stream) one by one
	;   test for cursors
	;   will have to handle backspace etc
	;   TODO it would also be cool to have a command autosuggestion popup as you type
	;   that would need a prefix tree (trie) structure
	mov edi, shell_command_buffer
	mov ebx, 0

.loop:
	cmp   ebx, BUF_CAP
	jge   .overflow
	mov   eax, stream
	ccall fgetc, eax
	cmp   eax, -1
	jz    .eof

	; TODO the escape stuff should prob be refactored from vga driver...

	cmp escape_mode, ESCAPE_MODE_ESCAPE
	je  .escape_mode
	cmp escape_mode, ESCAPE_MODE_CSI
	je  .csi_mode

	cmp al, ASCII_NEWLINE
	je  .end_loop

	; TODO check for esc chars

	cmp al, ASCII_ESCAPE
	je  .escape

	cmp al, ASCII_BACKSPACE
	je  .backspace

	cmp   dword echo_char, 0
	jz    .loop_inc
	push  eax
	ccall putchar, eax
	pop   eax

.loop_inc:
	stosb
	inc ebx
	jmp .loop

.eof:
	cmp edi, shell_command_buffer
	je  .return_eof
	;   we are just waiting for user input
	jmp .loop

.backspace:
	cmp   dword echo_char, 0
	jz    .clear_last_char
	ccall putchar, eax

.clear_last_char:
	dec edi
	mov byte [edi], NULL
	jmp .loop

.escape:
	mov escape_mode, ESCAPE_MODE_ESCAPE
	jmp .loop

.escape_mode:
	cmp al, CSI_DELIM_CHAR
	je  .csi

	; no other escape modes supported, just reset

	mov escape_mode, ESCAPE_MODE_NORMAL
	jmp .loop

.csi:
	mov escape_mode, ESCAPE_MODE_CSI
	jmp .loop

.csi_mode:
	cmp al, CSI_CURSOR_UP
	je  .csi_cursor_up

	; TODO support more CSI modes

	mov escape_mode, ESCAPE_MODE_NORMAL
	jmp .loop

.csi_cursor_up:
	std

.clear_current_command_loop:
	cmp edi, shell_command_buffer
	je  .get_history

	cmp   dword echo_char, 0
	jz    .del_char
	ccall putchar, ASCII_BACKSPACE

.del_char:
	mov al, NULL
	stosb
	jmp .clear_current_command_loop

.get_history:
	mov byte [edi], NULL
	cld

	; TODO this will only do one history, we should change history datastructure
	; to have pointer to end, as we need that to append

	mov ecx, esi
	dec ecx

	mov   eax, ecx
	mov   edx, BUF_CAP
	mul   edx
	mov   edi, history_strings
	add   edi, eax
	ccall strlen, edi
	push  eax
	ccall memcpy, shell_command_buffer, edi, eax
	pop   eax
	mov   edi, shell_command_buffer
	add   edi, eax
	mov   escape_mode, ESCAPE_MODE_NORMAL
	cmp   dword echo_char, 0
	jz    .loop
	ccall puts, shell_command_buffer

	jmp .loop

.overflow:
	; TODO if overflowing and not just newline, then we should fetch more data in new buffer

.end_loop:
	cmp   dword echo_char, 0
	jz    .null_terminate_str
	ccall putnl

.null_terminate_str:
	;   overwrite the newline with null
	mov al, NULL
	stosb
	inc ebx

	; add to history

	;     TODO make a function to append, and have history data structure have pointer
	;     to end, so we don't have to track through esi
	mov   ecx, esi
	cmp   ecx, HISTORY_CAP
	jge   .history_overflow
	;     TODO use malloc instead of history_strings
	mov   eax, ecx
	mov   edx, BUF_CAP
	mul   edx
	mov   edi, history_strings
	add   edi, eax
	push  ecx
	ccall memcpy, edi, shell_command_buffer, ebx
	pop   ecx
	mov   dword [history_list + WORD_SIZE*ecx], edi

.history_overflow:
	; TODO Overflow/wrap history

	mov ebx, shell_command_buffer

	ccall strtok, ebx, delimiters_str
	mov   edi, eax
	;     ebx - command (not trimmed)
	;     edi - command
	;     todo do something differently if prefixed by delim
	mov   ecx, [builtins]
	mov   esi, builtins+WORD_SIZE

.builtins_loop:
	test  ecx, ecx
	je    .non_builtin
	push  ecx
	ccall strcmp, edi, [esi]
	pop   ecx
	cmp   eax, 0
	jne   .builtins_loop_inc
	;     run command
	mov   eax, [esi+2*WORD_SIZE]
	call  eax
	jmp   .success

.builtins_loop_inc:
	add esi, 3*WORD_SIZE
	dec ecx
	jmp .builtins_loop

.non_builtin:
	; todo run some other cmd

	ccall printf, .unknown_command_printf_str, edi

	jmp .unknown
.unknown_command_printf_str: db "Unknown command: %s", 10, 0

.return_eof:
	mov eax, -1
	jmp .return

.unknown:
	;   TODO remove from history
	mov eax, 1
	jmp .return

.success:
	;   TODO add command to history
	;   will need to make a copy in history_strings
	;   strtok also would have removed spaces...
	mov eax, 0

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

old_termios equ var1
args_size = 1 * WORD_SIZE

shell_main:
	enter args_size, 0

	call shell_init

	ccall tcgetattr, STDIN_FILENO, .termios_p
	test  eax, eax
	jnz   .error

	ccall malloc, sizeof.termios
	test  eax, eax
	jz    .error

	mov   old_termios, eax
	ccall memcpy, eax, .termios_p, sizeof.termios

	mov eax, .termios_p
	mov edx, [termios.c_lflag]
	and edx, not ECHO and not ICANON
	mov [termios.c_lflag], edx

	ccall tcsetattr, STDIN_FILENO, TCSANOW, .termios_p
	test  eax, eax
	jnz   .error

.prompt:

	call display_prompt

.loop:
	ccall shell_process_command, stdin, 1
	cmp   eax, 0
	jge   .prompt
	jmp   .loop

	;     unreachable atm
	mov   eax, old_termios
	ccall tcsetattr, STDIN_FILENO, TCSANOW, eax
	test  eax, eax
	jnz   .error

.error:
	ccall puts, .error_str

.return:
	leave
	ret

.error_str:
	db "ERROR using tcgetattr", 13, 10, 0

.termios_p:
	termios_p termios

display_prompt:
	enter 0, 0
	ccall printf, ps1_str
	leave
	ret

	;; char *shell_next_arg()

shell_next_arg:
	enter 0, 0
	;     TODO this doesn't allow us to have '\ ' which would be useful...
	;     This relies on the fact that we use strtok to split from the first command
	;     FIXME in future, args will be parsed for us and put in an array somewhere?
	ccall strtok, NULL, delimiters_str

.return:
	leave
	ret

	BUILTINS_CAP = 30

builtins:
	dd    0
	times BUILTINS_CAP * 3 dd 0

	cmd_str equ arg1
	shortdoc_str equ arg2
	cmd equ arg3

shell_add_builtin:
	enter 0, 0
	mov   eax, [builtins]
	cmp   eax, BUILTINS_CAP
	jg    .error
	mov   ecx, 3 * WORD_SIZE
	mul   ecx
	add   eax, builtins
	add   eax, WORD_SIZE
	mov   edx, cmd_str
	mov   [eax], edx
	add   eax, WORD_SIZE
	mov   edx, shortdoc_str
	mov   [eax], edx
	add   eax, WORD_SIZE
	mov   edx, cmd
	mov   [eax], edx
	add   eax, WORD_SIZE
	inc   dword [builtins]
	jmp   .return

.error:
	ccall printf, .builtin_overflow_str
	extrn panic
	jmp   panic

.builtin_overflow_str:
	db "ERROR: Overflow in shell_add_builtin", 13, 10, 0

.return:
	leave
	ret

extrn run_tests

initialise_builtins:
	enter 0, 0
	ccall shell_add_builtin, help_cmd_str, help_shortdoc_str, help
	ccall shell_add_builtin, history_cmd_str, history_shortdoc_str, history
	ccall shell_add_builtin, noop_cmd_str, noop_shortdoc_str, noop
	ccall shell_add_builtin, echo_cmd_str, echo_shortdoc_str, echo
	ccall shell_add_builtin, printf_cmd_str, printf_shortdoc_str, shell_printf
	ccall shell_add_builtin, sleep_cmd_str, sleep_shortdoc_str, shell_sleep
	ccall shell_add_builtin, beep_cmd_str, beep_shortdoc_str, beep
	ccall shell_add_builtin, run_tests_cmd_str, run_tests_shortdoc_str, run_tests
	;     TODO add individual tests

.return:
	leave
	ret

history:
	enter 0, 0

	xor ecx, ecx

.history_loop:
	cmp ecx, HISTORY_CAP
	jge .return
	cmp dword [history_list + WORD_SIZE*ecx], 0
	je  .return

	mov   eax, ecx
	mov   edx, BUF_CAP
	mul   edx
	mov   edx, history_strings
	add   edx, eax
	push  ecx
	ccall puts, edx
	ccall putnl
	pop   ecx

	inc ecx
	jmp .history_loop

.return:

	leave
	ret

noop:
	ret

echo:
	enter 0, 0
	push  esi
	push  ebx
	xor   ebx, ebx

.loop:
	ccall shell_next_arg
	mov   esi, eax
	test  esi, esi
	jz    .return
	test  ebx, ebx
	je    .puts
	ccall putchar, ' '

.puts:
	ccall puts, esi
	inc   ebx
	jmp   .loop

.return:
	call putnl
	pop  ebx
	pop  esi
	leave
	ret

shell_printf:
	enter 0, 0
	push  ebx
	push  esi
	push  edi
	xor   ebx, ebx
	ccall shell_next_arg
	test  eax, eax
	jz    .usage
	mov   esi, eax

.parse_format_loop:
	mov  edx, esi
	xor  eax, eax
	lodsb
	test al, al
	je   .return
	cmp  al, '%'
	je   .parse_format
	cmp  al, '\'
	je   .parse_escape
	jmp  .putchar

.parse_format:
	;    TODO skip flags, field width, precision, length modifier
	;    FIXME parse * args
	lodsb
	test al, al
	je   .missing_format_error
	cmp  al, 'd'
	je   .integer_type
	cmp  al, 'i'
	je   .integer_type
	cmp  al, 'o'
	je   .integer_type
	cmp  al, 'u'
	je   .integer_type
	cmp  al, 'x'
	je   .integer_type
	cmp  al, 's'
	je   .string_type
	cmp  al, 'c'
	je   .char_type
	;    TODO %b and %q
	jmp  .invalid_format_error

.integer_type:
	mov   ebx, edx
	ccall shell_next_arg
	test  eax, eax
	je    .missing_argument_error
	ccall atoi, eax
	;     TODO detect error and print error about invalid number
	mov   edx, ebx
	jmp   .call_printf_with_format

.string_type:
	mov   ebx, edx
	ccall shell_next_arg
	test  eax, eax
	je    .missing_argument_error
	mov   edx, ebx
	jmp   .call_printf_with_format

.char_type:
	mov   ebx, edx
	ccall shell_next_arg
	test  eax, eax
	je    .missing_argument_error
	mov   edi, eax
	ccall strlen, eax
	test  eax, eax
	je    .missing_argument_error
	cmp   eax, 1
	jg    .char_too_long_error
	xor   eax, eax
	mov   al, [edi]
	mov   edx, ebx
	jmp   .call_printf_with_format

.call_printf_with_format:
	mov   bl, [esi]
	mov   byte [esi], NULL
	ccall printf, edx, eax
	mov   [esi], bl
	jmp   .parse_format_loop

.parse_escape:
	lodsb
	test al, al
	;    fixme, this could be backspace space, but next arg splits on whitespace, prob need to fix next_arg code

	;je   .missing_escape_code_error
	;     HACK: hack to do backslash space
	jne   .test_char
	ccall shell_next_arg
	cmp   eax, esi
	jne   .missing_escape_code_error
	xor   eax, eax
	mov   al, ' '
	mov   [esi-1], al

.test_char:
	;   todo, should this be in here, or swallowed by the shell arg parsing?
	cmp al, ' '
	je  .putchar
	cmp al, '"'
	je  .putchar
	cmp al, '\'
	je  .putchar
	cmp al, 'a'
	je  .putchar_bell
	cmp al, 'b'
	je  .putchar_backspace
	cmp al, 'c'
	je  .return; \c produces no further output
	cmp al, 'e'
	je  .putchar_escape
	cmp al, 'f'
	je  .putchar_form_feed
	cmp al, 'n'
	je  .putnl
	cmp al, 'r'
	je  .putchar_carriage_return
	cmp al, 't'
	je  .putchar_tab
	cmp al, 'v'
	je  .putchar_vertical_tab
	;   TODO \NNN, \xHH, \uHHHH, \UHHHHHHHH
	jmp .invalid_escape_code_error

.putchar:
	ccall putchar, eax
	jmp   .parse_format_loop

.putnl:
	ccall putnl
	jmp   .parse_format_loop

.putchar_bell:
	ccall putchar, ASCII_BELL
	jmp   .parse_format_loop

.putchar_backspace:
	ccall putchar, ASCII_BACKSPACE
	jmp   .parse_format_loop

.putchar_escape:
	ccall putchar, ASCII_ESCAPE
	jmp   .parse_format_loop

.putchar_form_feed:
	ccall putchar, ASCII_FORM_FEED
	jmp   .parse_format_loop

.putchar_carriage_return:
	ccall putchar, ASCII_CARRIAGE_RETURN
	jmp   .parse_format_loop

.putchar_tab:
	ccall putchar, ASCII_TAB
	jmp   .parse_format_loop

.putchar_vertical_tab:
	ccall putchar, ASCII_VERTICAL_TAB
	jmp   .parse_format_loop

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

.usage:
	ccall puts, .usage_str
	jmp   .return

.usage_str:
	db "printf: usage: printf format [arguments]", 13, 10, 0

.arg_overflow:
	ccall printf, .overflow_str, ebx
	jmp   .return

.overflow_str:
	db "printf: too many arguments, max %d", 13, 10, 0
	;  inner function, call it

.null_terminate:
	mov edi, esi
	mov al, NULL
	stosb
	ret

.missing_format_error:
	;     TODO print char count?
	call  .null_terminate
	ccall printf, .missing_format_error_str, edx
	jmp   .return

.missing_format_error_str:
	db "printf: '%s': missing format character", 13, 10, 0

.invalid_format_error:
	;     TODO print char count?
	call  .null_terminate
	ccall printf, .invalid_format_error_str, edx
	jmp   .return

.invalid_format_error_str:
	db "printf: '%s': invalid format character", 13, 10, 0

.missing_argument_error:
	;     TODO print char count?
	call  .null_terminate
	ccall printf, .missing_argument_error_str, edx
	jmp   .return

.missing_argument_error_str:
	db "printf: '%s': missing argument", 13, 10, 0

.char_too_long_error:
	;     TODO print char count?
	push  edi
	call  .null_terminate
	pop   edi
	ccall printf, .char_too_long_error_str, edx, edi
	jmp   .return

.char_too_long_error_str:
	db "printf: '%s': expected single character, got '%s'", 13, 10, 0

.missing_escape_code_error:
	;     TODO print char count?
	ccall printf, .missing_escape_code_error_str
	jmp   .return

.missing_escape_code_error_str:
	db "printf: '\': missing escape code", 13, 10, 0

.invalid_escape_code_error:
	;     TODO print char count?
	ccall printf, .invalid_escape_code_error_str, eax
	jmp   .return

.invalid_escape_code_error_str:
	db "printf: '\%c': invalid escape code", 13, 10, 0

shell_sleep:
	enter 0, 0
	ccall shell_next_arg
	test  eax, eax
	jz    .usage
	ccall atoi, eax
	;     TODO detect invalid numbers
	;     TODO detect suffixes
	ccall sleep, eax

.return:
	leave
	ret

.usage:
	ccall puts, .usage_str
	jmp   .return

.usage_str:
	db "sleep: missing time argument", 13, 10, 0

beep:
	enter 0, 0
	ccall putchar, ASCII_BELL

.return:
	leave
	ret

help:
	enter 0, 0
	push  ebx
	push  esi

	; todo see if we have an arg, and if so give more detailed help

	ccall puts, .help_str

	mov ebx, [builtins]
	mov esi, builtins + WORD_SIZE

.builtins_loop:
	test  ebx, ebx
	je    .return
	ccall printf, .help_printf_str, [esi], [esi+WORD_SIZE]

.builtins_loop_inc:
	add esi, 3*WORD_SIZE
	dec ebx
	jmp .builtins_loop

.return:
	pop esi
	pop ebx
	leave
	ret

.help_str:
	db "The following shell commands are builtin. Type `help` to see this list.", 13, 10
	db 13, 10
	db 0

	; width is 80 chars
	; TODO, used %-N.Ns to truncate longer strings, while still having min
	; this is currently not working in our printf

.help_printf_str:
	db " %-15s - %-60s", 13, 10, 0

shell_init:
	enter 0, 0

	ccall puts, .init_str

	ccall initialise_builtins

	;      todo, these aren't really builtins
	public shell_add_builtin
	extrn  add_test_builtins
	ccall  add_test_builtins
	public shell_next_arg
	extrn  music_add_builtin
	ccall  music_add_builtin
	extrn  sched_add_builtin
	ccall  sched_add_builtin

.autostart_command:
	ccall shell_process_command, shell_autostart_file, 0
	cmp   eax, 0
	jge   .autostart_command
	;eof

	; TODO clear history

.return:
	leave
	ret

.init_str:
	db "Thank you for trying out my hobby OS.", 13, 10
	db "If you have any issues, please report a bug at the following bug tracker:", 13, 10
	db 9, "https://gitlab.com/hughdavenport-osdev/osdev", 13, 10
	db 0

history_cmd_str:
	db "history", 0

history_shortdoc_str:
	db "History command.", 0

noop_cmd_str:
	db ":", 0

noop_shortdoc_str:
	db "Null command.", 0

echo_cmd_str:
	db "echo", 0

echo_shortdoc_str:
	db "Write arguments to the output.", 0

printf_cmd_str:
	db "printf", 0

printf_shortdoc_str:
	db "Print arguments with format.", 0

sleep_cmd_str:
	db "sleep", 0

sleep_shortdoc_str:
	db "Delay for specified time", 0

beep_cmd_str:
	db "beep", 0

beep_shortdoc_str:
	db "Make an audible beep", 0

run_tests_cmd_str:
	db "run_tests", 0

run_tests_shortdoc_str:
	db "Run full test suite.", 0

help_cmd_str:
	db "help", 0

help_shortdoc_str:
	db "Display information about builtin commands.", 0

ps1_str:
	db "> ", 0

shell_command_buffer:
	times BUF_CAP db 0

delimiters_str:
	db " ", 9, 13, 13, 0

history_strings:
	;     TODO remove this once we have malloc
	times HISTORY_CAP * BUF_CAP db 0

history_list:
	times HISTORY_CAP dd 0

shell_autostart_file:
	;    todo, this will be a `open` call at some point
	;    for now, read in at assembly time, and make a FILE struct
	dd   .eof-$-3*WORD_SIZE; current length of buf
	dd   0; index into buf for ring buffer
	dd   .eof-$-WORD_SIZE; capactiy
	file '../../.shellrc'

.eof:
