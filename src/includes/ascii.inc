	; ASCII constants for non printable chars

	ASCII_ESCAPE          = 0x1b
	ASCII_BELL            = 0x07
	ASCII_BACKSPACE       = 0x08
	ASCII_TAB             = 0x09
	ASCII_NEWLINE         = 0x0a
	ASCII_VERTICAL_TAB    = 0x0b
	ASCII_FORM_FEED       = 0x0c
	ASCII_CARRIAGE_RETURN = 0x0d
	ASCII_DELETE          = 0x7f
