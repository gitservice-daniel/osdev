	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  sched_add_builtin

	extrn shell_add_builtin

	include 'includes/kernel.inc'
	include 'lib/includes/stdlib.inc'
	include 'lib/includes/stdio.inc'

	STATE_UNKNOWN = 0
	STATE_RUNNING = 1
	STATE_PAUSED  = 2

	struc  thread_control_block {
	.tid   dd ?
	.pid   dd ?
	.esp   dd ?
	;      TODO may need a TSS and have esp0
	.cr3   dd ?; not used?
	.next  dd ?
	.state dd ?
	;      TODO accounting and prios

	}
	virtual at edi
	thread_control_block thread_control_block
	sizeof.thread_control_block = $ - thread_control_block
	end     virtual

	; TODO more posix'y name?
	; pthread_init or something?

initialise_multitasking:
	enter 0, 0

	push edi

	ccall malloc, sizeof.thread_control_block
	test  eax, eax
	je    .error
	mov   edi, eax

	mov [thread_control_block.tid], 1
	mov [thread_control_block.pid], 1
	mov [thread_control_block.esp], NULL; this will get set in switch_to_task
	mov eax, cr3
	mov [thread_control_block.cr3], eax
	mov [thread_control_block.next], NULL
	mov [thread_control_block.state], STATE_RUNNING

	mov [current_task], edi

	xor eax, eax
	jmp .return

.error:
	extrn panic
	ccall panic

.return:
	pop edi
	leave
	ret

	; TODO more posix'y name?
	; maybe pthread_create to make a new one? (and store id's in a hash?)
	start_routine equ arg1

new_task:
	enter 0, 0

	push ebx
	push edi

	ccall malloc, STACK_SIZE
	test  eax, eax
	je    .error
	mov   ebx, eax
	add   ebx, STACK_SIZE

	;   setup stack to match what switch_to_task expects
	mov eax, esp
	mov ecx, start_routine
	mov esp, ebx

	;    push EIP
	push ecx

	; TODO push enter frame if used in switch_to_task

	;     push ebx, esi, edi, ebp, used in switch_to_task
	times 4 push dword 0

	;   reset stack to this current task, set the task stack top in ebx
	mov ebx, esp
	mov esp, eax

	ccall malloc, sizeof.thread_control_block
	test  eax, eax
	je    .error
	mov   edi, eax

	mov [thread_control_block.tid], 2; TODO should probably not be hard coded
	mov [thread_control_block.pid], 1; TODO should probably find this out somehow
	mov [thread_control_block.esp], ebx
	mov eax, cr3
	mov [thread_control_block.cr3], eax
	mov eax, [current_task]
	mov [thread_control_block.next], eax
	mov [thread_control_block.state], STATE_PAUSED

	mov eax, edi
	mov edi, [current_task]

.loop:
	cmp [thread_control_block.next], NULL
	je  .set_next
	mov edi, [thread_control_block.next]
	jmp .loop

.set_next:
	mov [thread_control_block.next], eax
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop edi
	leave
	ret

	; TODO more posix'y name?
	; maybe sched_yield (man 7 sched)

	;;void switch_to_task(thread_control_block *next)
	;      NOTE, can't use arg1 macro for next, as not using enter/leave
	;      Adapted from "https://wiki.osdev.org/Brendan%27s_Multi-tasking_Tutorial"

switch_to_task:
	; don't use `enter`
	; TODO could use enter, just need to massage stack in new_task

	push ebx
	push esi
	push edi
	push ebp

	mov edi, [current_task]
	mov [thread_control_block.esp], esp

	mov edi, [esp + (4+1)*4]; thread_control_block *next arg
	mov [current_task], edi

	mov esp, [thread_control_block.esp]
	mov eax, [thread_control_block.cr3]
	;   TODO may need a TSS and have esp0
	mov ecx, cr3

	cmp eax, ecx
	je  .return
	mov cr3, eax

.return:
	pop ebp
	pop edi
	pop esi
	pop ebx
	;   don't use `leave`
	;   this will ret to new task's EIP
	ret

schedule:
	; todo do we need enter/leave?
	ret

sched_test:
	enter 0, 0

	;     FIXME this should prob be in kernel init code
	ccall initialise_multitasking

	ccall puts, .test_start_str

	ccall new_task, simple_task
	ccall simple_task
	ccall puts, .test_end_str

	leave
	ret

.test_start_str:
	db "Starting sched test, creating another task to run", 13, 10, 0

.test_end_str:
	db "Ended sched test", 13, 10, 0

simple_task:
	enter 0, 0

	push edi
	mov  edi, [current_task]

	mov ecx, 10

.loop:
	test  ecx, ecx
	jz    .return
	push  ecx
	ccall printf, .task_str, [thread_control_block.tid], ecx
	ccall switch_to_task, [thread_control_block.next]
	pop   ecx
	dec   ecx
	jmp   .loop

.return:
	pop edi
	leave
	ret

.task_str:
	db "In task %d, ecx = %d", 13, 10, 0

	; TODO at some point this will be an ID pthread_self returns and can be looked up in a hash

sched_add_builtin:
	enter 0, 0
	ccall shell_add_builtin, sched_cmd_str, sched_shortdoc_str, sched_test
	leave
	ret

sched_cmd_str:
	db "sched_test", 0

sched_shortdoc_str:
	db "Simple test of multitasking scheduler", 0

current_task:
	dd 0; pointer to current task structure
