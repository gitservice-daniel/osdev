	;; 32 bit protected mode
	use32

	extrn  kmain
	extrn  printf
	;      TODO better way to call driver init's
	extrn  vga_init
	extrn  pic8259_init
	extrn  ps2_keyboard_init
	extrn  pit_init
	public panic
	public halt
	public busy_loop

	include '../includes/kernel.inc'

	MAGIC = 0x1ABCDEF9
	IDT_SIZE = 32 + 16

boot32:
	;   Now in 32 bit protected mode
	;   Limitations on protected mode is no access to BIOS interrupts
	;   While in protected mode, can write to VGA text buffer
	jmp .start
dd int3_handler; Used to break in gdb when code calls int 3
dd busy_loop; Used by debugger to disable the busy loop

.start:
	mov  esp, kernel_stack_top
	;    TODO there is a chance that vga_init will fail if not all image is read
	call vga_init

	cmp   dword [magic], MAGIC
	je    .magic_correct
	pushd [magic]
	pushd MAGIC
	pushd magic
	push  magic_str
	call  printf
	add   esp, 4
	jmp   panic

.magic_correct:
	;    TODO better way to call driver init's
	call ps2_keyboard_init
	call idt_init
	call pit_init
	call pic8259_init
	sti

	call kmain
	push kmain_fallout_str
	call printf
	add  esp, 4
	;    fall through to panic if we return from kmain

panic:
panic32:
	;    TODO should reset scroll on vga?
	pusha
	push panic32_str
	call printf
	add  esp, 4
	push dump_regs_fmt_str
	call printf
	popa
	;    TODO: print out call stack
	;    TODO: print out memory?
	;    fall through to halt

halt:
halt32:
	cli
	hlt
	jmp halt

busy_loop:
busy_loop32:
	mov eax, BUSY_LOOP_SIZE

.loop:
	test eax, eax
	je   .return
	dec  eax
	jmp  .loop

.return:
	ret

include 'includes/idt.inc'

panic32_str:
	db "PANIC", 10, 0

kmain_fallout_str:
	db "UNREACHABLE: unexpected return from kmain", 10, 0

magic_str:
	db "ERROR: could not read all source code, magic is at %p, expected %#x, got %#x", 10, 0

dump_regs_fmt_str:
	db "EDI: %#010x, ESI: %#010x, EBP: %#010x, ESP: %#010x", 10
	db "EBX: %#010x, EDX: %#010x, ECX: %#010x, EAX: %#010x", 10
	db 0

	section '.magic'
	public  magic

magic:
	dd MAGIC
