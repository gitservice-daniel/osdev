	;      FIXME, currently we are in the bootloader, which I'm not sure is the right place for this
	public idt_add_trap_entry

	; Interrupt Descriptor Table

	;; Handlers

div_by_0:
int0_handler:
	ccall printf, div_by_0_str
	jmp   panic

breakpoint:
int3_handler:
	xchg bx, bx; trap for bochs
	iret

double_fault:
int8_handler:
	ccall printf, double_fault_str
	jmp   panic

	;; Initialize IDT

idt_init:
	lidt [idt_pointer]

	ccall idt_add_trap_entry, 0, int0_handler
	ccall idt_add_trap_entry, 3, int3_handler
	ccall idt_add_trap_entry, 8, int8_handler

	ret

irq     equ arg1
handler equ arg2

idt_add_trap_entry:
	enter 0, 0
	mov   ecx, irq
	shl   ecx, 3; mul 8
	add   ecx, idt_start
	cmp   ecx, idt_end
	jge   .error
	cmp   ecx, idt_start
	jl    .error
	mov   eax, handler
	mov   [ecx], ax; address lower 16 bits
	mov   word [ecx + 2], CODE_SEG; code segment
	mov   byte [ecx + 4], 0; reserved
	mov   byte [ecx + 5], 0x8E; 32bit trap gate, and present flag
	shr   eax, 16
	mov   word [ecx + 6], ax; address upper 16 bits
	leave
	ret

.error:
	mov   eax, irq
	ccall printf, idt_oob_str, eax
	jmp   panic

idt_start:
	times IDT_SIZE dq 0

idt_end:
idt_pointer:
	dw idt_end - idt_start
	dd idt_start

div_by_0_str:
	db 10, "EXCEPTION: Division By 0", 10, 0

double_fault_str:
	db 10, "EXCEPTION: Double Fault", 10, 0

idt_oob_str:
	db "ERROR: idt_add_trap_entry called with out of bounds index %d", 10, 0
