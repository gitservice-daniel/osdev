	;; 16 bit real mode
	use16

	; Master Boot Record (MBR) requires AA55 at end of the first 512 bytes
	MBR_SIZE  = 512
	MBR_MAGIC = 0xaa55
	MBR_ENTRY = 0x7c00
