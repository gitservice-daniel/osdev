	;; 16 bit real mode
	use16

enable_a20:
	;    Check whether A20 is enabled first (virtualbox already has it enabled)
	call check_a20
	cmp  ax, 1
	je   .return

	call bios_enable_a20
	;    ignore error code

	call check_a20
	cmp  ax, 1
	je   .return
	;    Couldn't enable it
	mov  si, a20_error_str
	call bios_print

.return:
	ret

check_a20:
	push ds

	xor ax, ax; ax = 0
	mov es, ax

	not ax; ax = 0xFFFF
	mov ds, ax

	mov di, 0x0500
	mov si, 0x0510

	mov  al, byte [es:di]
	push ax

	mov  al, byte [ds:si]
	push ax

	mov byte [es:di], 0x00
	mov byte [ds:si], 0xFF

	cmp byte [es:di], 0xFF

	pop ax
	mov byte [ds:si], al

	pop ax
	mov byte [es:di], al

	mov ax, 0
	je  check_a20__exit

	mov ax, 1

check_a20__exit:
	pop ds

	ret

a20_error_str:
	db "ERR: A20", 13, 10, 0
