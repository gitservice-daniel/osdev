	;; 16 bit real mode
	use16

	; BIOS functions used
	BIOS_A20               = 0x15
	BIOS_A20_CHECK_SUPPORT = 0x2403
	BIOS_A20_CHECK_GATE    = 0x2402
	BIOS_A20_ENABLE_GATE   = 0x2401

	BIOS_VIDEO                = 0x10
	BIOS_VIDEO_SET_MODE       = 0x00
	BIOS_VIDEO_MODE_VGA_TEXT  = 0x03
	BIOS_VIDEO_WRITE_CHAR_TTY = 0x0e

	BIOS_DISK              = 0x13
	BIOS_DISK_READ_SECTORS = 0x02
	BIOS_DISK_GET_PARAMS   = 0x08

	; Parameters
	BIOS_DISK_READ_RETRIES = 3

	;; void bios_enable_a20()

bios_enable_a20:
	;    The A20 gate allows us to read mode than 1MB
	mov  ax, BIOS_A20_CHECK_SUPPORT
	int  BIOS_A20
	jb   .return
	test ah, ah
	jnz  .return

	;    Check A20 gate status with BIOS
	mov  ax, BIOS_A20_CHECK_GATE
	int  BIOS_A20
	jb   .return
	test ah, ah
	jnz  .return

	cmp al, 1
	jz  .return

	;   Activate A20 gate
	mov ax, BIOS_A20_ENABLE_GATE
	int BIOS_A20

.return:
	ret

	;; int bios_disk_read()

bios_disk_read:
	push bx
	mov  dx, [MAGIC_OFFSET+2]
	mov  ax, [MAGIC_OFFSET]
	sub  ax, boot
	sbb  dx, 0
	mov  cx, 512
	div  cx
	mov  bx, ax; number of sectors to read
	mov  cx, 1; lba to start

.loop:
	test  bx, bx
	jle   .success
	mov   ax, bx
	mov   dx, 127
	cmp   ax, dx
	cmovg ax, dx
	push  cx
	push  cx; lba of sector to read
	push  ax; min(number of sectors, 127)
	call  bios_disk_read_sectors
	add   sp, 4
	pop   cx
	test  ax, ax
	jnz   .failure
	add   cx, 127
	sub   bx, 127
	jmp   .loop

.failure:
	mov  si, bios_disk_read_failure_str
	call bios_print
	mov  ax, 1
	jmp  .return

.success:
	mov ax, 0

.return:
	pop bx
	ret

	;; int bios_disk_read_sectors(num_sectors)

bios_disk_read_sectors:
	enter 0, 0
	push  bx
	push  es
	push  BIOS_DISK_READ_RETRIES

.loop:
	pop  ax
	test ax, ax
	je   .failure
	dec  ax
	push ax

	mov  ah, BIOS_DISK_GET_PARAMS
	mov  dl, [disk]
	int  BIOS_DISK
	inc  dh
	xchg dh, dl
	push dx; number of heads
	and  cl, 0x3f
	push cx; sectors per track

	;    These were changed to 8 bit divs. Hopefully this doesn't cause issues when we increase disk size
	mov  ax, [bp + 6]; lba
	add  ax, 62; offset to 0x7c00
	mov  cx, 512
	mul  cx
	pop  cx; sectors per track
	pop  bx; number of heads
	push dx; memory location
	push ax; memory location
	mov  ax, [bp + 6]; lba
	div  cl; sectors per head, from int call above
	inc  ah
	mov  cl, ah; sector idx
	xor  ah, ah
	div  bl; number of heads, from int call above
	mov  dh, ah; head idx
	mov  ch, al; cylinder idx
	pop  bx; memory location to store
	pop  ax
	shl  ax, 12; full address is es<<4+bx, so shift es<<12 to start
	mov  es, ax; memory location to store
	mov  al, [bp + 4]; number of sectors to read
	mov  ah, BIOS_DISK_READ_SECTORS
	mov  dl, [disk]; disk idx
	int  BIOS_DISK
	jnc  .success
	jmp  .loop

.failure:
	mov ax, 1
	jmp .return

.success:
	pop ax; counter
	mov ax, 0

.return:
	pop es
	pop bx
	leave
	ret

	;; void bios_print(char *)

bios_print:
	;   Doesn't use cdecl to save space, expects si to point to string to print
	;   si will be updated
	mov ah, BIOS_VIDEO_WRITE_CHAR_TTY

.loop:
	lodsb
	or  al, al
	jz  .return
	int BIOS_VIDEO
	jmp .loop

.return:
	ret

bios_print_number:
	;   Doesn't use cdecl to save space, expects ax to be num, cx to be base
	;   si and di will be updated
	mov di, .digit_buf

.loop:
	xor dx, dx
	div cx
	add dl, '0'
	cmp dl, '9'
	jle .stosb
	add dl, 'a' - '0' - 10; hex char

.stosb:
	xchg ax, dx
	stosb
	xchg ax, dx
	test ax, ax
	jz   .print
	jmp  .loop

.print:
	mov   si, di
	std
	lodsb ; undo the last stosb
	mov   ah, BIOS_VIDEO_WRITE_CHAR_TTY

.print_loop:
	lodsb
	int BIOS_VIDEO
	cmp si, .digit_buf
	jl  .return
	jmp .print_loop

.return:
	cld
	ret

.digit_buf:
	times 16 db 0; may not be NULL terminated, but we don't use it as a string

bios_disk_read_failure_str:
	db "ERR: disk", 13, 10, 0
