### Contributions

I welcome contributions in the form of:

*   [reporting issues or feature requests](https://gitlab.com/hughdavenport-osdev/osdev/-/issues/new)
*   [merge requests](https://gitlab.com/hughdavenport-osdev/osdev/-/merge_requests/new) for bug fixes or new features
*   Comments or constructive criticisms

## Issue reporting

TODO some documentation on what makes a good issue report

## Feature requests

TODO some documentation on what is likely or not to be accepted

## Development flow

TODO some documentation on what an ideal development flow is
