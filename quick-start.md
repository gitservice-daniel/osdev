## Quick Start

1.  Download the [latest release](https://gitlab.com/hughdavenport-osdev/osdev/-/releases/permalink/latest/downloads/osdev.img).
2.  Go to https://copy.sh/v86/
3.  Next to `Hard drive disk image`, click `Choose File`, and select the `osdev.img` file in the `build` directory.
4.  Click `Start Emulation`
